<?php
class TMv_WorkflowSettingsForm extends TSv_ModuleSettingsForm
{
	public function __construct($module)
	{	
		parent::__construct($module);
		
	}
	
	public function configureFormElements()
	{
		$field = new TCv_FormItem_Select('workflow_enabled', 'Enabled');
		$field->setHelpText("Indicate if Workflow is enabled. This allows the Workflow functionality to be turned off.");
		$field->addOption('1', "YES – Workflow Enabled");
		$field->addOption('0', "NO – Workflow Disabled");
		$this->attachView($field);

		$field = new TCv_FormItem_Select('send_emails', 'Send Emails');
		$field->setHelpText("Indicate if emails should be sent for notifications.");
		$field->addOption('1', "YES – Emails are sent to users");
		$field->addOption('0', "NO – Emails are NOT sent to users");
		$this->attachView($field);

		$field = new TCv_FormItem_Select('superuser_ids', 'Super-User IDs');
		$field->setHelpText('The comma-separated list of user IDs that are considered super-users which permit them to move an item from any stage, to any stage.');
		$field->setUseMultiple(',');
		
		$user_list = TMm_UserList::init();
		$field->useFiltering($user_list,'namesAndEmailsForSearchString', 'TMm_User');
		
		$this->attachView($field);

		$field = new TCv_FormItem_Select('permitted_user_groups', 'Permitted User Groups');
		$field->setHelpText("Indicate the user groups that can interact in Workflow.");
		$user_group_list = TMm_UserGroupList::init();
		foreach($user_group_list->models() as $user_group)
		{
			$field->addOption($user_group->id(), $user_group->title());
		}
		$field->setUseMultiple(',');
		$field->useFiltering();
		$this->attachView($field);


		// If Slack is installed
		if (TC_moduleWithNameInstalled('slack') && class_exists('TMm_SlackClient'))
		{
			$field = new TCv_FormItem_Heading('slack_heading', 'Slack Settings');
			$this->attachView($field);

			$field = new TCv_FormItem_Select('slack_for_comments', 'Slack for Comments');
			$field->setHelpText("Indicate if comments should trigger Slack notifications.");
			$field->addOption('1', "YES – Send Slack notifications for comments");
			$field->addOption('0', "NO – Do NOT send Slack notifications for comments");
			$this->attachView($field);

//			$field = new TCv_FormItem_Select('slack_for_comment_resolutions', 'Slack for Comment Resolutions');
//			$field->setHelpText("Indicate if comments being marked as resolved, should trigger Slack notifications.");
//			$field->addOption('1', "YES – Send Slack notifications for comment resolutions");
//			$field->addOption('0', "NO – Do NOT send Slack notifications for comment resolutions");
//			$this->attachView($field);

			$field = new TCv_FormItem_Select('slack_for_stages', 'Slack for Stage Changes');
			$field->setHelpText("Indicate if stage changes should trigger Slack notifications.");
			$field->addOption('1', "YES – Send Slack notifications for stage changes");
			$field->addOption('0', "NO – Do NOT send Slack notifications for stage changes");
			$this->attachView($field);

			$field = new TCv_FormItem_Select('slack_for_user_assignment', 'Slack for User Assignment');
			$field->setHelpText("Indicate if user assignments should trigger Slack notifications.");
			$field->addOption('1', "YES – Send Slack notifications for user assignments");
			$field->addOption('0', "NO – Do NOT send Slack notifications for user assignments");
			$this->attachView($field);


		}

	}
	
}
?>