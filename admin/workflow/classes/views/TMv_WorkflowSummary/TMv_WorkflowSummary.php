<?php
class TMv_WorkflowSummary extends TCv_View
{
	//protected $workflow_config = false;
	protected $workflow_model = false;
	protected $workflow_item = false;
	protected $workflow_module;
	protected $edit_url = '';

	/**
	 * TMv_WorkflowSummaryColumn constructor.
	 * @param TMm_WorkflowItem $workflow_item
	 */
	public function __construct($workflow_item)
	{
		$this->workflow_item = $workflow_item;
		parent::__construct('workflow_summary_'.$this->workflow_item->id());
		$this->addClass('workflow_box');
		if(TC_moduleWithNameInstalled('workflow'))
		{
			$this->workflow_module = TMm_WorkflowModule::init();
		
			$this->workflow_model = $workflow_item->model();
			
			$model_module = TC_moduleForClass($this->workflow_model->baseClassName());

			$module_folder = $model_module->folder();
			$url_target_name = $this->workflow_model->adminEditURLTargetName();
			$this->edit_url = '/'.TC_getConfig('Tungsten_admin_folder_URL').'/'.$module_folder.'/do/'.$url_target_name.'/'.$this->workflow_model->id();

			$this->addClassCSSFile();

		}

	}


	public function render()
	{
		if(!$this->workflow_module)
		{
			return '';
		}

		$stage_model = $this->workflow_item->currentStageModel();
	//	$this->addConsoleDebugObject($this->workflow_item->model()->contentCode(), $this->workflow_item);
		$current_stage = $this->workflow_item->currentStageModel()->stage();
		$stage = $stage_model->stage();

		// ---- STAGE LINE -----
		$stage_color = new TCv_Link();
		$stage_color->addClass('workflow_stage_color');
		
		$workflow_tag = new TCv_View();
		$workflow_tag->setTag('span');
		$workflow_tag->addClass('workflow_tag');
		$workflow_tag->setAttribute('style','background-color:#'.$stage->workflow()->color().';');
		$workflow_tag->addText($stage->workflow()->code());
		$stage_color->attachView($workflow_tag);
		
		$stage_color->setAttribute('style', 'background:#' . $stage->color() . ';color:#'.$stage->readableColor().';');
		$stage_color->setTitle($current_stage->title());
		$stage_color->setURL('#');
		$stage_color->setAttribute('onclick',
		                           "$('#workflow_summary_".$this->workflow_item->id()." .next_stage_box').slideToggle(); return false;");
		$stage_color->addText($stage->title());
		$this->attachView($stage_color);


		if($stage->userCanMoveStage())
		{

			$next_stage_box = new TCv_View('next_stage_box_' . $this->workflow_model->contentCode());
			$next_stage_box->addClass('next_stage_box');
			$next_stage_box->setAttribute('style', 'border-color:#' . $stage->color());
			
			
			foreach($stage->nextStages() as $next_stage)
			{
				$button = new TCv_Link();
				$button->addClass('next_stage_button');
				$button->setAttribute('style', 'border-color:#' . $next_stage->color() . ';');
				$button->setURL('/admin/workflow/do/change-workflow-stage/' . $stage_model->id() . '/' . $next_stage->id());
				$button->setIconClassName('fa-chevron-right');
				$button->addText($next_stage->title());
				$next_stage_box->attachView($button);

			}
			$this->attachView($next_stage_box);
		}


		if($current_stage->userCanMoveStage())
		{
			$next_stage_box = new TCv_View();
			$next_stage_box->addClass('next_stage_box');

		}

		if($stage->title() != 'Done')
		{


			$workflow_details_box = new TCv_View();
			$workflow_details_box->setTag('span');
			$workflow_details_box->addClass('workflow_details');


			// ---- USERS -----
			$users = $this->workflow_module->usersForModel($this->workflow_model);
			
			foreach($users as $user)
			{
				$box = new TCv_Link();
				//$box->setTag('span');
				$box->setURL($this->edit_url . '?workflow-people');
				$box->addClass('detail_box');
				$box->addClass('user_box');
				$box->addText($user->user()->initials());
				if($user->id() == TC_currentUser()->id())
				{
					$box->addClass('active_user');
				}
				$workflow_details_box->attachView($box);
			}


			// ---- COMMENTS -----
			
			$comments = $this->workflow_module->commentsForModel($this->workflow_model);
			$num_comments = sizeof($comments);
			
			
			$num_unresolved_comments = 0;
			foreach($comments as $comment)
			{
				if(!$comment->isResolved())
				{
					$num_unresolved_comments++;
				}
			}

//			$this->addConsoleDebug($num_unresolved_comments);
//			$this->addConsoleDebug($num_comments);

			if($num_comments > 0)
			{
				$box = new TCv_Link();
				$box->setURL($this->edit_url . '?workflow-comments');
				$box->addClass('detail_box');
				$box->addClass('comment_box');
				$box->setIconClassName('fa-comments');


				if($num_unresolved_comments == 0)
				{
					$box->addText('All');
				}
				else
				{
					$box->addText(($num_comments - $num_unresolved_comments) . ' of ' . $num_comments);
					$box->addClass('comment_action_required');
				}

				$box->addText(' Resolved');
				$workflow_details_box->attachView($box);
			}


			$this->attachView($workflow_details_box);

		}
	}


	/**
	 * @param string $url_target_name
	 * @param string $title
	 * @param string $icon_name
	 * @param bool|string $target_id
	 */
	public function addError($url_target_name, $title, $icon_name, $target_id = false)
	{
		$module = $this->workflow_model->moduleForThisClass();

		// We have a successful link to create
		$link = new TCv_Link();
		$link->setTitle( $title );
		$link->addText('<span>'.$title.'</span>');
		$link->setURL('/'.TC_getConfig('Tungsten_admin_folder_URL').'/'.$module->folder().'/do/'.$url_target_name.'/'
					  .$this->workflow_model->id());



		//$link = $this->linkForModuleURLTargetName($this, $url_target_name);
		$link->setIconClassName($icon_name);
		if($target_id)
		{
			$link->appendToURL('#'.$target_id);
		}
		$link->addClass('workflow_error');
		$this->attachView($link);
	}



	public function html()
	{

		return parent::html();
	}



}
?>