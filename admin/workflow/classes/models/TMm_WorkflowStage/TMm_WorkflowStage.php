<?php
class TMm_WorkflowStage extends TCm_Model
{
	protected $title, $description, $color, $display_order, $readable_color, $workflow_id;
	protected $user_groups = false;
	protected $next_stages = false;
	
	protected $color_light, $color_dark;
	
	protected $items = null;
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'stage_id'; // [string] = The id column for this class
	public static $table_name = 'workflow_stages'; // [string] = The table for this class
	public static $model_title = 'Stage';
	public static $primary_table_sort = 'display_order ASC';
	
	public function __construct($id)
	{
		parent::__construct($id);
	}


	/**
	 * Returns the title for this stage
	 * @return string
	 */
	public function title()
	{
	//	return 'x';
		return $this->title;
	}

	/**
	 * Returns the color hex code for this stage
	 * @return string
	 */
	public function color()
	{
		return $this->color;
	}
	
	/**
	 * Returns the readable color based on the overall contrast of the main color chosen
	 * @return string
	 */
	public function readableColor()
	{
		// https://stackoverflow.com/questions/3015116/hex-code-brightness-php
		if($this->readable_color == null)
		{
			$r = hexdec(substr($this->color, 0, 2));
			$g = hexdec(substr($this->color, 2, 2));
			$b = hexdec(substr($this->color, 4, 2));
			
			$squared_contrast = (
				$r * $r * .299 +
				$g * $g * .587 +
				$b * $b * .114
			);
			
			if($squared_contrast > pow(150, 2))
			{
				$this->readable_color = $this->colorDark();
			}
			else
			{
				$this->readable_color = 'FFFFFF';//$this->colorLight();
			}
		}
		
		return $this->readable_color;
	}
	
	function shadeColor2($color, $percent)
	{
		$color = str_replace("#", "", $color);
		$t = $percent < 0 ? 0 : 255;
		$p = $percent < 0 ? $percent * -1 : $percent;
		$RGB = str_split($color, 2);
		$R = hexdec($RGB[0]);
		$G = hexdec($RGB[1]);
		$B = hexdec($RGB[2]);
		return '#' . substr(dechex(0x1000000 + (round(($t - $R) * $p) + $R) * 0x10000 + (round(($t - $G) * $p) + $G) * 0x100 + (round(($t - $B) * $p) + $B)), 1);
	}
	
	
	/**
	 * Returns the color that is lighter for this workflow
	 * @return string
	 */
	public function colorLight()
	{
		if($this->color_light == null)
		{
			$this->color_light = $this->shadeHexColor($this->color,-80);
		}

		return $this->color_light;
	}
	
	/**
	 * Returns the color that is lighter for this workflow
	 * @return string
	 */
	public function colorDark()
	{
		if($this->color_dark == null)
		{
			$this->color_dark = $this->shadeHexColor($this->color,75);
		}
		
		return $this->color_dark;
	}
	
	
	
	/**
	 * Returns the list of user groups that are associated with this stage
	 * @return TMm_UserGroup[]
	 */
	public function userGroups()
	{
		if($this->user_groups === false)
		{
			$this->user_groups = array();

			// CHECK FOR USER GROUP MATCHES
			if($this->id() > 0)
			{
				$query = "SELECT * FROM workflow_stage_user_group_matches m INNER JOIN user_groups g USING(group_id) WHERE m.stage_id = :stage_id";
				$result = $this->DB_Prep_Exec($query, array('stage_id' => $this->id()));
				while($row = $result->fetch())
				{
					$group = TC_initClass('TMm_UserGroup', $row);
					$this->user_groups[$group->id()] = $group;
				}
			}

		}

		return $this->user_groups;
	}

	/**
	 * Returns if this user has permission to move this stage
	 * @param bool|TMm_User $user
	 * @return bool
	 */
	public function userCanMoveStage($user = false)
	{
		if($user === false)
		{
			$user = TC_currentUser();
		}

		// Admins can always move a stage
		if($user->isAdmin())
		{
			return true;
		}

		foreach($this->userGroups() as $user_group)
		{
			if($user->inGroup($user_group))
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Updates a single group value to indicate if the user is a part of this group or not. This function co-incides
	 * with the required function for the TCv_FormItem_CheckboxList class.
	 * @param TMm_UserGroup $user_group
	 * @param bool $is_member
	 */
	public function updateGroupSetting($user_group, $is_member)
	{
		$current_member = $this->groupHasAccess($user_group);

		// Current Member that shouldn't be
		if($current_member && !$is_member)
		{
			$query = "DELETE FROM workflow_stage_user_group_matches WHERE stage_id = :stage_id AND group_id = :group_id";
			$result = $this->DB_Prep_Exec($query, array('group_id'=> $user_group->id(),':stage_id' => $this->id()));
			unset($this->user_groups[$user_group->id()]);
		}

		// NOT A MEMBER
		elseif(!$current_member && $is_member)
		{
			$query = "INSERT INTO workflow_stage_user_group_matches SET stage_id = :stage_id , group_id = :group_id, date_added = now()";
			$result = $this->DB_Prep_Exec($query, array('group_id'=> $user_group->id(),'stage_id' => $this->id()));
			$this->user_groups[$user_group->id()] = $user_group;

		}
	}

	/**
	 * Updates the list of user groups based on an associative array of values
	 * @param $values
	 */
	public function updateUserGroups($values)
	{
		$user_group_list = TC_initClass('TMm_UserGroupList',false);
		foreach($user_group_list->groups() as $user_group)
		{
			$in_group = in_array($user_group->id(), $values);
			$this->updateGroupSetting($user_group, $in_group);
		}
	}

	/**
	 * Returns if the provided user group is associated with this stage
	 * @param TMm_UserGroup $user_group
	 * @return bool
	 */
	public function groupHasAccess($user_group)
	{
		$this->userGroups();
		return isset($this->user_groups[$user_group->id()]);
	}

	//////////////////////////////////////////////////////
	//
	// NEXT STAGES
	//
	// A stage can only transition to other stages
	//
	//////////////////////////////////////////////////////



		/**
	 * Returns the list of stages that this stage can transition to
	 * @return TMm_WorkflowStage[]
	 */
	public function nextStages()
	{
		if($this->next_stages === false)
		{
			$this->next_stages  = array();

			// CHECK FOR USER GROUP MATCHES
			if($this->id() > 0)
			{
				$query = "SELECT * FROM workflow_stage_next_stage_matches m INNER JOIN workflow_stages s ON(m.next_stage_id = s.stage_id) WHERE m.stage_id = :stage_id ORDER BY s.display_order";
				$result = $this->DB_Prep_Exec($query, array('stage_id' => $this->id()));
				while($row = $result->fetch())
				{

					$stage = TC_initClass('TMm_WorkflowStage', $row);

					// Never Yourself
					if($stage->id() != $this->id())
					{
						$this->next_stages[$stage->id()] = $stage;
					}
				}
			}

		}

		return $this->next_stages ;
	}


	/**
	 * Updates a single stage value to indicate if this stage uses that stage as a next stage. This function co-incides
	 * with the required function for the TCv_FormItem_CheckboxList class.
	 * @param TMm_WorkflowStage $stage
	 * @param bool $is_member
	 */
	public function updateNextStageSetting($stage, $is_member)
	{
		$is_set = $this->stageIsPossibleNext($stage);

		// IS SET
		if($is_set && !$is_member)
		{
			$query = "DELETE FROM workflow_stage_next_stage_matches WHERE stage_id = :stage_id AND next_stage_id = :next_stage_id";
			$this->DB_Prep_Exec($query, array('next_stage_id'=> $stage->id(),':stage_id' => $this->id()));
			unset($this->next_stages[$stage->id()]);
		}

		// NOT Set
		elseif(!$is_set && $is_member)
		{
			$query = "INSERT INTO workflow_stage_next_stage_matches  SET stage_id = :stage_id , next_stage_id = :next_stage_id, date_added = now()";
			$this->DB_Prep_Exec($query, array('next_stage_id'=> $stage->id(),'stage_id' => $this->id()));
			$this->next_stages[$stage->id()] = $stage;

		}
	}

	/**
	 * Updates the list of next stages based on an associative array of values
	 * @param $values
	 */
	public function updateNextStages($values)
	{
		/** @var TMm_WorkflowStageList $stage_list */
		$stage_list = TC_initClass('TMm_WorkflowStageList',false);
		foreach($stage_list->models() as $stage)
		{
			$isset = in_array($stage->id(), $values);
			$this->updateNextStageSetting($stage, $isset);
		}
	}

	/**
	 * Returns if the provided stage is a possible next stage
	 * @param TMm_WorkflowStage $stage
	 * @return bool
	 */
	public function stageIsPossibleNext($stage)
	{
		$this->nextStages();
		return isset($this->next_stages[$stage->id()]);
	}

	//////////////////////////////////////////////////////
	//
	// MODELS IN STAGES
	//
	// Functions related to the models that belong in this stage.
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns all the models of a certain model name that are currently in this stage
	 * @param string $model_name
	 * @return int
	 */
	public function numItemsWithModelName($model_name)
	{
		$model_name = trim($model_name);
		$model_list_name = $model_name.'List';

		$query = "SELECT s.* FROM workflow_model_stages s 
		
		
		INNER JOIN (
			SELECT max(date_added) max_post_date, model_id FROM workflow_model_stages WHERE model_name = :model_name GROUP BY model_id
		) s2
		ON s.date_added = s2.max_post_date AND s.model_id = s2.model_id

		INNER JOIN ".$model_name::$table_name." m ON s.model_id = m.".$model_name::$table_id_column."

		WHERE s.model_name = :model_name AND s.stage_id = :stage_id ORDER BY s.date_added ASC




";
		$result = $this->DB_Prep_Exec($query, array('stage_id' => $this->id(), 'model_name' => $model_name));
		return $result->rowCount();
	}

	public function workflowItems()
	{
		if($this->items == null)
		{
			$this->items =array();
			$query = "SELECT * FROM workflow_items WHERE stage_id = :stage_id ORDER BY display_order ASC";
			
			
		}
		return $this->items();
	}
	
	/**
	 * Returns the workflow for this item
	 * @return bool|TMm_Workflow
	 */
	public function workflow()
	{
		return TMm_Workflow::init($this->workflow_id);
	}



}
?>