<?php
class TMm_WorkflowConfig extends TCm_Model
{
	/** @var bool|TCm_Model */
	protected $workflow_model = false;
	protected $comments = false;
	protected $model_stages  = false;
	protected $users = false;
	
	protected $workflow_models = array();

	/** @var bool|TMm_WorkflowModelStage */
	protected $current_model_stage  = false;

	/**
	 * TMm_WorkflowConfig constructor.
	 * @param bool|TCm_Model $workflow_model
	 */
	public function __construct($workflow_model = false)
	{
		parent::__construct('workflow_config');

		$this->addConsoleError('TMm_WorkflowConfig');
		// only bother if workflow is installed
		if(TC_moduleWithNameInstalled('workflow') && TC_getModuleConfig('workflow', 'workflow_enabled') )
		{
			
			$this->workflow_models = explode(',', TC_getModuleConfig('workflow', 'model_class_names'));
			foreach($this->workflow_models as $index => $model_name)
			{
				$this->workflow_models[$index] = trim($model_name);
				
			}
			if ($workflow_model instanceof TCm_Model)
			{
				$this->workflow_model = $workflow_model;
			
			}
			else
			{
				foreach (TC_activeModels() as $active_model)
				{
					foreach ($this->workflow_models as $model_name)
					{
						if ($active_model instanceof $model_name)
						{
							$this->workflow_model = $active_model;
						}
					}
				}
			}
			
			if(!$this->workflow_model)
			{
				// this is a list of items
			}
		}
	}
	
	/**
	 * Returns if the provided model object or model name is workflow enabled
	 * @param string|TCm_Model $model
	 * @return bool
	 */
	public function modelIsWorkflowEnabled($model)
	{
		foreach($this->workflow_models as $model_name)
		{
			if($model instanceof $model_name)
			{
				return true;
			}
			elseif(is_string($model) && $model == $model_name)
			{
				return true;
			}
		}
		return false;
	}
	

	/**
	 * Returns if a workflow is enabled for the current page being viewed
	 * @return bool
	 */
	public function workflowEnabled()
	{
		return $this->workflow_model !== false;
	}

	/**
	 * Returns the model name for the workflow model
	 * @return string
	 */
	public function modelName()
	{
		return $this->workflow_model->baseClassName();
	}

	/**
	 * Returns the model name for the workflow id
	 * @return string
	 */
	public function modelID()
	{
		return $this->workflow_model->id();
	}

	/**
	 * Returns the list of comments, starting with the newest ones
	 * @return TMm_WorkflowComment[]
	 */
	public function comments()
	{
		if($this->comments === false)
		{
			$this->comments = array();
			$query = "SELECT * FROM workflow_model_comments WHERE model_name = :model_name AND model_id = :model_id ORDER BY date_added DESC";
			$result = $this->DB_Prep_Exec($query, array('model_name' => $this->modelName(), 'model_id' => $this->modelID()));
			while($row = $result->fetch())
			{
				$this->comments[] = TC_initClass('TMm_WorkflowComment', $row);
			}

		}

		return $this->comments;

	}

	/**
	 * Returns the unresolved comment
	 * @return TMm_WorkflowComment[]
	 */
	public function unresolvedComments()
	{
		$comments = array();
		foreach($this->comments() as $comment)
		{
			if(!$comment->isResolved())
			{
				$comments[] = $comment;
			}
		}

		return $comments;
	}

	//////////////////////////////////////////////////////
	//
	// MODEL USERS
	//
	// The list of users associated with a given item
	//
	//////////////////////////////////////////////////////

	/**
	 * Returns the list of users
	 * @return TMm_User[]
	 */
	public function users()
	{
		if($this->users === false)
		{
			$this->users= array();
			$query = "SELECT users.* FROM workflow_model_users INNER JOIN users USING(user_id) WHERE model_name = :model_name AND model_id = :model_id ORDER BY last_name, first_name DESC";
			$result = $this->DB_Prep_Exec($query, array('model_name' => $this->modelName(), 'model_id' => $this->modelID()));
			while($row = $result->fetch())
			{
				$user = TMm_User::init( $row);
				$this->users[$user->id()] = $user;
			}

		}
		return $this->users;

	}

	/**
	 * Toggles if this user is associated with the given model
	 * @param TMm_User $user
	 */
	public function toggleUser($user)
	{
		$this->users();
		$values = array();
		$values['model_name'] = $this->modelName();
		$values['model_id'] = $this->modelID();
		$values['user_id'] = $user->id();

		if(!isset($this->users[$user->id()]))
		{
			$workflow_user = TMm_WorkflowUser::createWithValues($values);
			$this->users[$user->id()] = $user;
		}
		else // exists
		{
			$query = "DELETE FROM workflow_model_users WHERE model_name = :model_name AND model_id = :model_id AND user_id = :user_id";
			$this->DB_Prep_Exec($query, $values);

			unset($this->users[$user->id()]);
		}
	}

	
	//////////////////////////////////////////////////////
	//
	// MODEL STAGES
	//
	// Models have a history of the stages they were added
	// to and this track them as well as returns the most recent
	//
	//////////////////////////////////////////////////////


	/**
	 * Returns the current stage for this item
	 * @return TMm_WorkflowModelStage
	 */
	public function currentStageModel()
	{
		$this->modelStages(); // init the list
		return $this->current_model_stage;
	}

	/**
	 * Returns the current stage for this item
	 * @return TMm_WorkflowStage
	 */
	public function currentStage()
	{
		$stages = $this->modelStages(); // init the list
		if(sizeof($stages) > 0)
		{
			return $this->current_model_stage->stage();
		}
		return false;
	}

	/**
	 * Returns the list of stages that have ever been assigned to this item
	 * @return TMm_WorkflowModelStage[]
	 */
	public function modelStages()
	{
		if($this->model_stages === false)
		{
			$this->model_stages  = array();

			$module = TMm_WorkflowModule::init();
			
			if($module->hasStagesForModel($this->workflow_model))
			{
				$this->model_stages = $module->stagesForModel($this->workflow_model);
				if(count($this->model_stages) > 0)
				{
					$this->current_model_stage = array_values($this->model_stages)[0];
				}
			}
			else
			{
				$query = "SELECT * FROM workflow_model_stages WHERE model_name = :model_name AND model_id = :model_id ORDER BY date_added DESC";
				$result = $this->DB_Prep_Exec($query, array('model_name' => $this->modelName(), 'model_id' => $this->modelID()));
				while($row = $result->fetch())
				{
					$model_stage = TMm_WorkflowModelStage::init($row);
					
					if($this->current_model_stage  === false)
					{
						$this->current_model_stage = $model_stage;
					}
					
					$this->model_stages [] = $model_stage;
				}
			}
			
			

		}

		// No Model Stage yet, assign the first one
		if($this->current_model_stage  === false)
		{
			$stage_list = TMm_WorkflowStageList::init();
			$first_stage = $stage_list->firstStage();

			$values = array();
			$values['model_name'] = $this->modelName();
			$values['model_id'] = $this->modelID();
			$values['user_id'] = TC_currentUser()->id();

			// ONly bother if there's actually a first stage
			if($first_stage)
			{
				$values['stage_id'] = $first_stage->id();
				$model_stage = TMm_WorkflowModelStage::createWithValues($values);
				$this->model_stages[] = $model_stage;
				$this->current_model_stage = $model_stage;
			}



		}

		return $this->model_stages ;

	}

	/**
	 * @return TMm_WorkflowHistoryItem[]
	 */
	public function recentlyItems()
	{
		$items = array();

		return $items;
	}

}
?>