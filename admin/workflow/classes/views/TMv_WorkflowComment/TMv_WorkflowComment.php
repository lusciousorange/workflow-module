<?php

/**
 * Class TMv_WorkflowComment
 */
class TMv_WorkflowComment extends TMv_WorkflowHistoryItem
{

	/**
	 * TMv_WorkflowComment constructor.
	 * @param TMm_WorkflowComment $comment
	 */
	public function __construct ($comment)
	{
		parent::__construct($comment);
	}

	/**
	 * @return bool|TMm_WorkflowComment
	 */
	public function item()
	{
		return parent::item();
	}

	protected function controlPanel()
	{
		$view = new TCv_View();
		$view->addClass('workflow_control_panel');

//		$link = new TCv_Link();
//		$link->addClass('comment_link');
//		$link->setURL('#');
//		$link->setIconClassName('fa-external-link-square');
//		$link->addDataValue('comment-id', $this->item()->id());
//		$link->setTitle('Link Comment To Element');
//		$view->attachView($link);

		$link = new TCv_Link();
		$link->addClass('comment_toggle_resolved');
		$link->setURL('#');
		$link->setIconClassName('fa-check-circle');
		$link->addDataValue('comment-id', $this->item()->id());
		$link->setTitle('Resolve This Comment');
		$view->attachView($link);

		$link = new TCv_Link();
		$link->addClass('comment_delete');
		$link->setURL('#');
		$link->setIconClassName('fa-trash');
		$link->setTitle('Delete This Comment');
		$link->addDataValue('comment-id', $this->item()->id());
		$view->attachView($link);


		return $view;
	}

	public function html ()
	{
		$user = $this->item()->user();

		$this->attachView($this->controlPanel());

		$this->addClassCSSFile('TMv_WorkflowComment?v1');

		if($this->item()->isResolved())
		{
			$this->addClass('resolved');
		}


		if($this->showItemTitle())
		{
			$this->attachView($this->modelLink());
		}

		$box = new TCv_View();
		$box->addClass('history_user');
		$box->addText($user->fullName());

		$this->attachView($box);

		$stage_box = new TCv_View();
		$stage_box->addClass('history_date');
		$stage_box->addText($this->item()->dateAddedFormatted('F j, Y \a\t g:ia'));
		$this->attachView($stage_box);


		$box = new TCv_View();
		$box->addClass('comment_box');
		$box->addText($this->item()->content());
		$this->attachView($box);

		if($this->item()->isLinked())
		{
			$icon = new TCv_View();
			$icon->addClass('content_link_icon');
			$icon->addClass('fa-link');
			$this->attachView($icon);

			$this->addDataValue('highlight-target', $this->item()->linkTargetID());
			$this->addClass('has_content_link');
		}




		return parent::html();
	}

}