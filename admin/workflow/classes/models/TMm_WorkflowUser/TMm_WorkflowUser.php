<?php
class TMm_WorkflowUser extends TMm_WorkflowHistoryItem
{
	protected $workflow_id, $user_id;
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'match_id'; // [string] = The id column for this class
	public static $table_name = 'workflow_model_users'; // [string] = The table for this class
	public static $model_title = 'User';
	public static $primary_table_sort = 'date_added DESC';

	public function __construct($id)
	{
		parent::__construct($id);
	}
	
	/**
	 * Returns the workflow for this item
	 * @return bool|TMm_Workflow
	 */
	public function workflow()
	{
		return TMm_Workflow::init($this->workflow_id);
	}
	
	
	
	/**
	 * Extends the functionality to also send slack messages if necessary
	 * @param array $values
	 * @param array $bind_param_values
	 * @param bool $return_new_model (Optional) Default true. Indicates if the newly created object should be
	 * instantiated and returned. This is a heuristic option to speed up complex calls where the model isn't needed
	 * @return bool|TCm_Model
	 */
	public static function createWithValues($values, $bind_param_values = array(), $return_new_model = true)
	{

		$new_user_match = parent::createWithValues($values, $bind_param_values);

//		// Loop through and find out if user wants to receive the email
//		$user = $new_user_match->user();
//		if($user->valueForProperty('workflow_email_assigned'))
//		{
//			$email = new TMv_WorkflowPeopleEmail($new_user_match, $user);
//			$email->send();
//		}
//
//
//
//		// HANDLE SLACK CALLS
//
//		// If Slack is installed
//		if(
//			TC_moduleWithNameInstalled('slack') && // Only bother if Slack is installed
//			class_exists('TMm_SlackClient') && // We also need the proper class
//			TC_getModuleConfig('workflow','slack_for_user_assignment') // Respect system pref for Slack and Workflow
//		)
//		{
//			/** @var TMm_SlackClient $slack_client */
//			$slack_client = TC_initClass('TMm_SlackClient');
//
//			$model_title = $new_user_match->model()->title();
//
//
//			$title_text = $slack_client->slackCodeForUser($new_user_match->user()). " has been added to the " . $new_user_match->typeTitle() ." : <".
//				$slack_client->fullDomainName().$new_user_match->model()->adminEditURL()
//				."|".$slack_client->cleanTextForSlack($model_title).'>';
//
//			$slack_client->addText($title_text);
//
//
//			$slack_client->sendIncomingWebhook();
//
//
//		}
		return $new_user_match;
	}

	public static function numModelsWithNameForUser($model_name, $user)
	{
		$model = new TCm_Model(false);
		$query = "SELECT * FROM workflow_model_users WHERE user_id = :user_id AND model_name = :model_name";
		$result = $model->DB_Prep_Exec($query, array('user_id' => $user->id(), 'model_name' => $model_name));
		return $result->rowCount();
	}

}
?>