<?php
class TMv_WorkflowItemEditor extends TCv_View
{
	protected $workflow_item;

	public function __construct ($workflow_item)
	{
		parent::__construct();
		$this->workflow_item = $workflow_item;
	}

	public function render ()
	{
		$this->attachView($this->tabBar());

		$this->attachView(TMv_WorkflowItemCommentBox::init($this->workflow_item));
		$this->attachView(TMv_WorkflowItemStatusBox::init($this->workflow_item));
		$this->attachView(TMv_WorkflowItemPeopleBox::init($this->workflow_item));
	}

	protected function tabBar()
	{
		$tab_bar = new TCv_View();
		$tab_bar->addClass('tab_bar');

		$comment_button = new TCv_Link('workflow_control_status');
		$comment_button->addText('Status');
		$comment_button->setIconClassName('fa-list-ol');
		$comment_button->addClass('control_button');
		$comment_button->addClass('wf_button');
		$comment_button->addDataValue('section', 'status');
		$comment_button->setURL('#');
		$tab_bar->attachView($comment_button);

		$comment_button = new TCv_Link('workflow_control_comments');
		$comment_button->addText('Comments');
		$comment_button->setIconClassName('fa-comments');
		$comment_button->addClass('control_button');
		$comment_button->addClass('wf_button');
		$comment_button->addDataValue('section', 'comments');
		$comment_button->setURL('#');
		$tab_bar->attachView($comment_button);

		$comment_button = new TCv_Link('workflow_control_people');
		$comment_button->addText('People');
		$comment_button->setIconClassName('fa-users');
		$comment_button->addClass('control_button');
		$comment_button->addClass('wf_button');
		$comment_button->addDataValue('section', 'people');
		$comment_button->setURL('#');
		$tab_bar->attachView($comment_button);

		return $tab_bar;
	}
}