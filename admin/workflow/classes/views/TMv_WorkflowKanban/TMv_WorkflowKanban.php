<?php
class TMv_WorkflowKanban extends TCv_View
{
	protected $workflow;
	
	/**
	 * TMv_WorkflowKanban constructor.
	 * @param TMm_Workflow $workflow
	 */
	public function __construct($workflow)
	{
		parent::__construct();
		
		$this->addClassCSSFile('TMv_WorkflowKanban');
		
		$this->workflow = $workflow;
	}
	
	public function render()
	{
		
		
		$inner_container = new TCv_View();
		$inner_container->addClass('inner_container');
		foreach($this->workflow->stages() as $stage)
		{
			$inner_container->attachView($this->stageView($stage));
			
		}
		
		$outer_container = new TCv_View();
		$outer_container->addClass('outer_container');
		$outer_container->attachView($inner_container);
		
		$this->attachView($outer_container);
	}
	
	/**
	 * @param TMm_WorkflowStage $stage
	 * @return TCv_View
	 */
	public function stageView($stage)
	{
		$stage_box = new TCv_View();
		$stage_box->addClass('stage_box');
		$stage_box->setAttribute('style','background-color:#'.$stage->colorLight().';');
		
		$stage_title_box = new TCv_View();
		$stage_title_box->addClass('stage_title_box');
		$stage_title_box->addText('<h2>'.$stage->title().'</h2>');
		$stage_title_box->setAttribute('style','background-color:#'.$stage->color().';color:#'.$stage->readableColor().';');
		
		$stage_box->attachView($stage_title_box);
		
		$stage_items = new TCv_View();
		$stage_items->addClass('stage_items');
		
		foreach($this->workflow->workflowItemsForStage($stage) as $workflow_item)
		{
			$stage_items->attachView($this->workflowItemView($workflow_item));
		}
		$stage_box->attachView($stage_items);
		
		return $stage_box;
	}

	/**
	 * @param TMm_WorkflowItem $workflow_item
	 * @return bool|TSv_ModuleURLTargetLink
	 */
	public function workflowItemView($workflow_item)
	{
		$url_target = TC_URLTargetFromModule('workflow', 'workflow-item-editor');
		$url_target->setModel($workflow_item);

		$link = TSv_ModuleURLTargetLink::init($url_target);

		$link = new TSv_ModuleURLTargetLink($url_target);
		$link->loadViewInDialog();
		$link->addClass('workflow_item_box');
		$link->clear();

		$card_title = new TCv_View();
		$card_title->setTag('h3');
		$card_title->addText($workflow_item->title());
		$link->attachView($card_title);

		return $link;
	}
}