<?php
class TMv_WorkflowItemPeopleBox extends TCv_View
{
	protected $workflow_item;

	/** @var bool|TMm_WorkflowModule $workflow_module  */
	protected $workflow_module;

	/**
	 * TMv_WorkflowItemCommentBox constructor.
	 *
	 * @param TMm_WorkflowItem $workflow_item
	 */
	public function __construct ($workflow_item)
	{
		$this->workflow_item = $workflow_item;
		$this->workflow_module = TMm_WorkflowModule::init();

		// ID uses main workflow for matching
		parent::__construct('workflow_people_box_'.$workflow_item->workflow()->id());
		$this->addClass('workflow_section_box');
		$this->addDataValue('workflow-id',$workflow_item->workflow()->id());

		if($this->workflow_item->model())
		{
			$params = array(
				'model_name' => $this->workflow_item->model()->baseClassName(),
				'model_id' => $this->workflow_item->model()->id(),

			);
		}

		//$this->addClassCSSFile('TMv_WorkflowItemPeopleBox');
		$this->addClassJQueryFile('TMv_WorkflowItemPeopleBox');
		$this->addClassJQueryInit('TMv_WorkflowItemPeopleBox', $params);


	}

	public function render ()
	{
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText('People');
		$this->attachView($heading);


		$form = new TCv_Form('workflow_people_form');
		$form->hideSubmitButton();
		$form->addDataValue('workflow-id', $this->workflow_item->workflow()->id());
		$permitted_user_groups = TC_getModuleConfig('workflow','permitted_user_groups');

		// TODO: This doesn't work for the standalone page

		// User Groups Set
		$users = array();
		if($permitted_user_groups != '')
		{
			$group_ids = explode(',', $permitted_user_groups);
			foreach($group_ids as $group_id)
			{
				$group = TMm_UserGroup::init($group_id);
				foreach($group->users() as $user)
				{
					$users[$user->id()] = $user;
				}
			}
		}
		else
		{
			$user_list = TMm_UserList::init();
			$users = $user_list->usersInAnyGroup();
		}


		$field = new TCv_FormItem_CheckboxList('users', 'Users');
		$field->setShowTitleColumn(false);
		$field->setSaveToDatabase(false);

		$selected_users = array();
		foreach($this->workflow_item->workflowUsers() as $workflow_user)
		{
			$selected_users[] = $workflow_user->user();
		}
		$field->setValuesFromObjectArrays($users, $selected_users);

		$form->attachView($field);

		$this->attachView($form);
	}

}