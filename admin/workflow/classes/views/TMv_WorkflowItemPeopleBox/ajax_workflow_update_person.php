<?php
	require($_SERVER['DOCUMENT_ROOT'] . "/admin/system/headers/tungsten_header.php");

//	foreach($_POST as $index => $value)
//	{
//		print '<br />'. $index .' = ' .$value;
//	}

	$module = TMm_WorkflowModule::init();
	$workflow_model = ($_POST['model_name'])::init($_POST['model_id']);
	$workflow = TMm_Workflow::init($_POST['workflow_id']);
	$workflow_user = $module->workflowUserForModelAndWorkflow($workflow_model, $workflow);

	$delete = false;

	if($workflow_user && $workflow_user->user()->id() == $_POST['user_id'])
	{
		$delete = true;
	}

	if($delete)
	{
		$workflow_user->delete(false); // false to not show a message

	}
	else
	{
		$values = array();
		$values['model_name'] = $_POST['model_name'];
		$values['model_id'] = $_POST['model_id'];
		$values['workflow_id'] = $_POST['workflow_id'];
		$values['user_id'] = $_POST['user_id'];
		TMm_WorkflowUser::createWithValues($values);

	}







?>