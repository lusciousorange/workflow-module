<?php

/**
 * Class TMv_WorkflowEditor
 */
class TMv_WorkflowEditor extends TCv_View
{
	
	/** @var bool|TMm_WorkflowModule  */
	protected $workflow_module = false;
	
	/** @var TCm_Model $model */
	protected $model;
	protected $workflow_items;
	
	public function __construct ($model)
	{
		parent::__construct('workflow_editor');
		
		$this->workflow_module = TMm_WorkflowModule::init();
		$this->model = $model;
		$this->workflow_items = $this->workflow_module->itemsForModel($model);
	}
	

	public function html ()
	{
		if(!$this->workflow_module->workflowEnabled())
		{
			return '';
		}

		// Add for comments so we can just pull them in
		$this->addClassCSSFile('TMv_WorkflowStageHistory');
		$this->addClassCSSFile('TMv_WorkflowHistoryItem?v1');
		$this->addClassCSSFile('TMv_WorkflowComment?v2');

		$model = $this->workflow_module->currentModel();
		
		if($model)
		{
			$params = array(
				'model_name' => $model->baseClassName(),
				'model_id' => $model->id(),
			
			);
		}
		
		$this->addClassCSSFile('TMv_WorkflowEditor?v2');
		$this->addClassJQueryFile('TMv_WorkflowEditor?v3');
		$this->addClassJQueryInit('TMv_WorkflowEditor', $params);


		$stage_box = new TCv_View();
		$stage_box->addClass('workflow_stage_box');

		// Not Setup Yet, probably no stages, avoid hard fail
		foreach($this->workflow_items as $workflow_item)
		{
			if($workflow_item->currentStageModel() && $workflow_item->isEnabled() )
			{

				$this->attachView(TMv_WorkflowItemCommentBox::init($workflow_item));
				$this->attachView(TMv_WorkflowItemStatusBox::init($workflow_item));
				$this->attachView(TMv_WorkflowItemPeopleBox::init($workflow_item));
				
			}
		}
		
		if($this->workflow_module->currentUserIsSuper())
		{
			$this->attachView($this->settingsBox());
		}

		return parent::html();
	}



	
	/**
	 * The box for settings
	 * @return TCv_View
	 */
	public function settingsBox()
	{
		$view = new TCv_View('workflow_settings_box');
		$view->addClass('workflow_section_box');
		
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText('Settings');
		$view->attachView($heading);
		
		foreach($this->workflow_module->workflows() as $workflow)
		{
			
			$workflow_box = new TCv_View();
			$workflow_box->addClass('workflow_box');
			$workflow_box->setAttribute('style','border-color:#'.$workflow->color().';');
			$workflow_heading = new TCv_View();
			$workflow_heading->setTag('h3');
			$workflow_heading->addText($workflow->title());
			$workflow_heading->setAttribute('style','background-color:#'.$workflow->color().';');
			$workflow_box->attachView($workflow_heading);
			
			//$workflow_box->addText('<h3>'.$workflow->title().'</h3>');
			
			$enabled = false;
			
			if($workflow_item = $this->workflow_module->workflowItemForModelAndWorkflow($this->model, $workflow))
			{
				$enabled = $workflow_item->isEnabled();
			}
			
			if($enabled)
			{	//$disable_button
				$disable_button = new TCv_Link();
				$disable_button->setIconClassName('fa-minus');
				$disable_button->addText('Disable workflow');
				$disable_button->addClass('disable_workflow_button');
				$disable_button->addClass('workflow_setting_button');
				$disable_button->setURL('/admin/workflow/do/disable-workflow-item/'.$workflow_item->id());
				$workflow_box->attachView($disable_button);
			}
			else
			{
				$enable_button = new TCv_Link();
				$enable_button->setIconClassName('fa-plus');
				$enable_button->addText('Enable workflow');
				$enable_button->addClass('enable_workflow_button');
				$enable_button->addClass('workflow_setting_button');
				$enable_button->setURL(
					'/admin/workflow/do/add-workflow-to-model/'.
					$workflow->id().'/'.$this->model->baseClassName().'/'.$this->model->id());
				$workflow_box->attachView($enable_button);
			}
			
			$view->attachView($workflow_box);
		}

		return $view;
	}

}