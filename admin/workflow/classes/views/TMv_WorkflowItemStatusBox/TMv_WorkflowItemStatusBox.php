<?php
class TMv_WorkflowItemStatusBox extends TCv_View
{
	protected $workflow_item;

	/** @var bool|TMm_WorkflowModule $workflow_module  */
	protected $workflow_module;

	/**
	 * TMv_WorkflowItemCommentBox constructor.
	 *
	 * @param TMm_WorkflowItem $workflow_item
	 */
	public function __construct ($workflow_item)
	{
		$this->workflow_item = $workflow_item;
		$this->workflow_module = TMm_WorkflowModule::init();

		// ID uses main workflow for matching
		parent::__construct('workflow_status_box_'.$workflow_item->workflow()->id());
		$this->addClass('workflow_section_box');
		$this->addDataValue('workflow-id',$workflow_item->workflow()->id());

	}

	public function render ()
	{
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText('Workflow Stage');
		$this->attachView($heading);

		$stage_model = $this->workflow_item->currentStageModel();
		$stage = $stage_model->stage();

		$stage_box = new TCv_View('stage_box_'.$this->workflow_item->workflow()->id());
		$stage_box->addClass('stage_box');
		$stage_box->addDataValue('workflow-id', $this->workflow_item->workflow()->id());
		$stage_box->setAttribute('style','background-color:#'.$stage->color().'; color:#'.$stage->readableColor().';');
		$stage_box->addText($stage->title());
		$this->attachView($stage_box);

		$this->attachView(new TMv_WorkflowStageProgressBar($stage));

		if($stage->userCanMoveStage() || $this->workflow_module->currentUserIsSuper())
		{
			$heading = new TCv_View();
			$heading->setTag('h3');
			$heading->addText('Move To Stage');
			$this->attachView($heading);

			$stages = $stage->nextStages();

			if($this->workflow_module->currentUserIsSuper())
			{
				$stages = $this->workflow_item->workflow()->stages();
			}



			foreach($stages as $next_stage)
			{
				if($stage->id() != $next_stage->id())
				{
					$button = new TCv_Link();
					$button->addClass('next_stage_button');
					$button->setAttribute('style', 'border-color:#' . $next_stage->color() . '; ');
					$button->setURL('/admin/workflow/do/change-workflow-stage/' . $stage_model->id() . '/' . $next_stage->id());
					$button->setIconClassName('fa-chevron-right');
					$button->addText($next_stage->title());
					$this->attachView($button);
				}
			}


		}

		$heading = new TCv_View();
		$heading->setTag('h3');
		$heading->addText('History');
		$this->attachView($heading);

		foreach($this->workflow_item->stageModelHistory() as $model_stage)
		{
			$history_view = new TMv_WorkflowStageHistory($model_stage);

			$this->attachView($history_view);

		}
	}

}