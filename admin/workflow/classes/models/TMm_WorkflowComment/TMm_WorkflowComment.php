<?php
class TMm_WorkflowComment extends TMm_WorkflowHistoryItem
{
	protected $comment, $is_resolved, $content_id, $workflow_id;

	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'comment_id'; // [string] = The id column for this class
	public static $table_name = 'workflow_model_comments'; // [string] = The table for this class
	public static $model_title = 'Comment';
	public static $primary_table_sort = 'date_added DESC';

	public function __construct($id)
	{
		parent::__construct($id);
	}


	public function content()
	{
		return $this->comment;
	}

	public function comment()
	{
		return $this->comment;
	}

	public function user()
	{
		return TMm_User::init( $this->user_id);
	}

	public function toggleResolved()
	{
		$this->updateDatabaseValue('is_resolved', !$this->is_resolved);
		$this->is_resolved = !$this->is_resolved;
	}
	
	/**
	 * Returns the workflow for this item
	 * @return bool|TMm_Workflow
	 */
	public function workflow()
	{
		return TMm_Workflow::init($this->workflow_id);
	}
	
	
	
	public function isResolved()
	{
		return $this->is_resolved;
	}

	/**
	 * Returns if this item is linked to a content item
	 * @return bool
	 */
	public function isLinked()
	{
		return $this->content_id > 0;
	}

	public function linkTargetID()
	{
		if($this->content_id > 0)
		{
			return $this->content_id;
		}


	}
	
	
	/**
	 * Extends the functionality to also send slack messages if necessary
	 * @param array $values
	 * @param array $bind_param_values
	 * @param bool $return_new_model
	 * @return bool|TCm_Model
	 */
	public static function createWithValues ($values, $bind_param_values = array(), $return_new_model = true)
	{
		$model = new TCm_Model(false);

		if(trim($values['comment']) == '')
		{
			return false;
		}

		/** @var TMm_WorkflowComment $new_comment */
		$new_comment = parent::createWithValues($values, $bind_param_values, $return_new_model);

		// Loop through and find out if user wants to receive the email
		foreach($new_comment->people() as $user)
		{
			if($user->valueForProperty('workflow_email_comments'))
			{
				$email = new TMv_WorkflowPeopleEmail($new_comment, $user);
				$email->send();
			}

		}

		// HANDLE SLACK CALLS

		// If Slack is installed
		if(
			TC_moduleWithNameInstalled('slack') && // Only bother if Slack is installed
			class_exists('TMm_SlackClient') && // We also need the proper class
			TC_getModuleConfig('workflow','slack_for_comments') // Respect system pref for Slack and Workflow
		)
		{

			/** @var TMm_SlackClient $slack_client */
			$slack_client = TC_initClass('TMm_SlackClient');

			$model_title = $new_comment->model()->title();
			$comment_text = $new_comment->comment();


			$title_text = $slack_client->cleanTextForSlack(TC_currentUser()->fullName()). " added a comment to " . $new_comment->typeTitle() ." : <".
				$slack_client->fullDomainName().$new_comment->model()->adminEditURL()
				."|".$slack_client->cleanTextForSlack($model_title).'>';

			$slack_client->addText($title_text);

			// Find list of people
			$people_names = array();
			foreach($new_comment->people() as $person)
			{
				$people_names[] = $slack_client->slackCodeForUser($person);
			}

			$attachment = array(
				"fallback" => "",
				"color" => '#640064', // Workflow Purple
				"fields" => array(
					array(
						"title" => "Comment",
						"value" => $slack_client->cleanTextForSlack($comment_text)
					),
					array(
						"title" => "People",
						"value" => implode(', ',$people_names)
					)

				)
			);
			$slack_client->addAttachment($attachment);
			$slack_client->sendIncomingWebhook();


		}
		return $new_comment;
	}

}
?>