tinyMCE.init(
	{
		selector : ".tiny_mce.workflow_comment_box textarea",
		theme : "modern",
		height : "150",
		plugins: [
	    	"advlist autolink lists link paste ",
	    ],
		toolbar1: "bold italic | link | bullist numlist ",
		statusbar: false,
		menubar : false,
		relative_urls: false,
	}
);