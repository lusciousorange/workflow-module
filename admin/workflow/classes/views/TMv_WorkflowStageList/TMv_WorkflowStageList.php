<?php
class TMv_WorkflowStageList extends TCv_RearrangableModelList
{
	protected $workflow;
	/**
	 * TMv_WorkflowStageList constructor.
	 * @param TMm_Workflow $workflow
	 */
	public function __construct($workflow)
	{
		parent::__construct();
		
		$this->setModelClass('TMm_WorkflowStage');
		if($workflow)
		{
			$this->addModels($workflow->stages());
		}
		
		$this->defineColumns();

		$this->addClassCSSFile();
	}

	public function defineColumns()
	{
		$column = new TCv_ListColumn('title');
		$column->setTitle('Title');
		$column->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('color');
		$column->setTitle('Color');
		$column->setContentUsingListMethod('colorColumn');
		$this->addTCListColumn($column);

		$column = new TCv_ListColumn('groups');
		$column->setTitle('Exit Control Groups');
		$column->setContentUsingListMethod('groupsColumn');
		$this->addTCListColumn($column);

		$column = new TCv_ListColumn('next_stages');
		$column->setTitle('Possible Next Stages');
		$column->setContentUsingListMethod('nextStagesColumn');
		$this->addTCListColumn($column);
//
//
//		$column = new TCv_ListColumn('location');
//		$column->setTitle('Date');
//		$column->setContentUsingListMethod('locationColumn');
//		$this->addTCListColumn($column);
//
//		$edit_button = $this->controlButtonColumnWithListMethod('visitIconColumn');
//		$edit_button->setWidthAsPixels(100);
//		$edit_button->setTitle('Visit');
//		$edit_button->setAlignment('center');
//		$this->addTCListColumn($edit_button);

		$edit_button = $this->controlButtonColumnWithListMethod('editIconColumn');
		$this->addTCListColumn($edit_button);
	
		$delete_button = $this->controlButtonColumnWithListMethod('deleteIconColumn');
		$this->addTCListColumn($delete_button);
				
	}


	/**
	 * Returns the column value for the provided model
	 * @param TMm_WorkflowStage $model
	 * @return TCv_View|string
	 */
	public function titleColumn($model)
	{
		$link = $this->linkForModuleURLTargetName($model, 'stage-edit');
		$link->addText($model->title());
		return $link;
	}

	/**
	 * Returns the column value for the provided model
	 * @param TMm_WorkflowStage $model
	 * @return TCv_View|string
	 */
	public function colorColumn($model)
	{
		$view = new TCv_View();
		$view->setTag('span');
		$view->addClass('color_box');
		$view->setAttribute('style','background-color:#'.$model->color().';color:#'.$model->readableColor().';');
		
		$view->addText('#' . $model->color());

		return $view;

	}

	/**
	 *
	 * @param TMm_WorkflowStage $model
	 * @return TCv_View|string
	 */
	public function groupsColumn($model)
	{
		$view = new TCv_View();
		foreach($model->userGroups() as $user_group)
		{
			$view->addText($user_group->title().'<br />');
		}
		return $view;
	}


	/**
	 *
	 * @param TMm_WorkflowStage $model
	 * @return TCv_View|string
	 */
	public function nextStagesColumn($model)
	{
		$view = new TCv_View();
		foreach($model->nextStages() as $stage)
		{
			$stage_box = new TCv_View();
			$stage_box->setTag('span');
			$stage_box->addClass('stage_box');
			$stage_box->setAttribute('style','background-color:#'.$stage->color().';color:#'.$stage->readableColor().';');
			
			
			
			$stage_box->addText($stage->title());
			$view->attachView($stage_box);


		//	$view->addText($stage->title().'<br />');
		}
		return $view;
	}


	public function editIconColumn($model)
	{
		return $this->listControlButton($model, 'stage-edit', 'fa-pencil');
		
	}
	
	public function deleteIconColumn($model)
	{
		return $this->listControlButton_Confirm($model, 'stage-delete', 'fa-trash');
	
	}
	

	
		
}
?>