(function ($)
{
// INIT
	$.fn.TMv_WorkflowItemPeopleBox = function(options)
	{

		var settings = $.extend(
			{
				model_name : false,
				model_id : false,
			}, options || {});


		function personClicked(event)
		{
			event.preventDefault();

			console.log(event);
			var $checkbox = $(event.currentTarget);

			var is_checked = $checkbox.is(':checked');

			var url_data = new Array();
			url_data.push('model_name'+ '=' + encodeURI(settings.model_name));
			url_data.push('model_id'+ '=' + encodeURI(settings.model_id));
			url_data.push('user_id'+ '=' + encodeURI($checkbox.val()));
			url_data.push('is_checked'+ '=' + (is_checked ? '1' : '0'));

			var workflow_id = $(event.target).closest('form').attr('data-workflow-id');
			url_data.push('workflow_id'+ '=' + encodeURI(workflow_id));

			console.log(url_data);
			jQuery.ajax(
				{
					url: "/admin/workflow/classes/views/TMv_WorkflowItemPeopleBox/ajax_workflow_update_person.php",
					type: "POST",
					data: url_data.join('&'),
					success: function(data)
					{


					}

				});


		}



		return this.each(function()
		{
			$('#workflow_people_form input[type="checkbox"]').change(function(event) { personClicked(event)});


		});


};


// END THE WRAPPER
})(jQuery);
