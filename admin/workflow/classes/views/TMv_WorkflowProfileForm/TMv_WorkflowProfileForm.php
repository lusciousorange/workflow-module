<?php

/**
 * Class TMv_WorkflowProfileForm
 *
 * The form that shows personal information
 */
class TMv_WorkflowProfileForm extends TMv_ProfileForm
{
	/**
	 * TMv_ProfilePersonalForm constructor.
	 */
	public function __construct()
	{	
		parent::__construct();
		
	}
	
	/**
	 * Configure to ask for name and email
	 */
	public function configureFormElements()
	{
		$field = new TCv_FormItem_Select('workflow_email_comments', 'Comment Emails');
		$field->setHelpText("A setting that indicates if emails are sent to you for comments being added in the workflow.");
		$field->addOption('1', "YES – Email me when comments are added to items I'm assigned to");
		$field->addOption('0', "NO – Do NOT send me emails when comments are added to items I'm assigned to");
		$this->attachView($field);

		$field = new TCv_FormItem_Select('workflow_email_stages', 'Stage Emails');
		$field->setHelpText("A setting that indicates if emails are sent to you for stage changes in the workflow.");
		$field->addOption('1', "YES – Email me when a stage is changed for items I'm assigned to");
		$field->addOption('0', "NO – Do NOT send me emails when a stage is changed for  items I'm assigned to");
		$this->attachView($field);

		$field = new TCv_FormItem_Select('workflow_email_assigned', 'Assignment Emails');
		$field->setHelpText("A setting that indicates if emails are sent to you when you are assigned or unassigned in the workflow.");
		$field->addOption('1', "YES – Email me when comments are added to items I'm assigned to");
		$field->addOption('0', "NO – Do NOT send me emails when comments are added to items I'm assigned to");
		$this->attachView($field);

		$this->setButtonText('Update Your Workflow Settings');
	}



}