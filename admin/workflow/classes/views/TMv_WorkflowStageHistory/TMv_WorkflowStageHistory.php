<?php

/**
 * Class TMv_WorkflowStageHistory
 */
class TMv_WorkflowStageHistory extends TMv_WorkflowHistoryItem
{
	protected $model_stage = false;
	/**
	 * TMv_WorkflowComment constructor.
	 * @param TMm_WorkflowModelStage $model_stage
	 */
	public function __construct ($model_stage)
	{

		parent::__construct($model_stage);

		$this->model_stage = $model_stage;

		$this->addClassCSSFile('TMv_WorkflowStageHistory');

	}

	public function html ()
	{
		$user = $this->model_stage->user();

		if($this->showItemTitle())
		{
			$this->attachView($this->modelLink());
		}

		$button = new TCv_View();
		$button->setTag('span');
		$button->addClass('workflow_stage_color');
		$button->setAttribute('style','background:#'.$this->model_stage->stage()->color().';color:#'
		                             .$this->model_stage->stage()->readableColor().';');
		$button->addText($this->model_stage->stage()->title());
		$this->attachView($button);
		
		$this->setAttribute('style','border-color:#'.$this->model_stage->stage()->color().';');

//		// ---- STAGE LINE -----
//		$stage_color = new TCv_Link();
//		$stage_color->addClass('workflow_stage_color');
//		$stage_color->setAttribute('style','background:#'.$stage->color().';');
//		$stage_color->setTitle($this->workflow_config->currentStage()->title());
//		$stage_color->setURL('#');
//		$stage_color->setAttribute('onclick',"$('#next_stage_box_".$this->workflow_model->contentCode()."').slideToggle(); preventDefault(); ");
//		$stage_color->addText($stage->title());
//		$this->attachView($stage_color);
//
//
//
//		$stage_box = new TCv_View();
//		$stage_box->addClass('stage_title');
//		$stage_box->addText($this->model_stage->stage()->title());
//		$this->attachView($stage_box);


		$stage_box = new TCv_View();
		$stage_box->addClass('history_user');
		$stage_box->addText($this->model_stage->user()->fullName());
		$this->attachView($stage_box);

		$stage_box = new TCv_View();
		$stage_box->addClass('history_date');
		$stage_box->addText($this->model_stage ->dateAddedFormatted('F j, Y \a\t g:ia'));
		$this->attachView($stage_box);



		return parent::html();
	}

}