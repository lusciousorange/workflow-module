<?php
class TMm_Workflow extends TCm_Model
{
	protected $title, $description, $is_user_restricted, $code, $model_class_names, $model_classes, $color;
	
	protected $color_light;
	
	protected $stages = null;
	
	protected $workflow_items_by_stage;
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'workflow_id'; // [string] = The id column for this class
	public static $table_name = 'workflows'; // [string] = The table for this class
	public static $model_title = 'Workflow';
	public static $primary_table_sort = 'title ASC';
	
	public function title()
	{
		return $this->title;
	}
	
	public function code()
	{
		return strtoupper($this->code);
	}
	
	public function color()
	{
		return $this->color;
	}
	
	/**
	 * Returns the color that is lighter for this workflow
	 * @return string
	 */
	public function colorLight()
	{
		if($this->color_light == null)
		{
			$this->color_light = $this->shadeHexColor($this->color,-85);
		}
		return $this->color_light;
	}
	
	public function titleWithCode()
	{
		return $this->code.' : '.$this->title();
	}
	
	/**
	 * Returns the array of model classes for this workflow
	 * @return string[]
	 */
	public function modelClassNames()
	{
		if($this->model_classes == null)
		{
			$this->model_classes = array();
			$classes =  explode(',', $this->model_class_names);
			foreach($classes as $class)
			{
				$class = trim($class);
				if($class != '')
				{
					$this->model_classes[] = $class;
				}
				
			}
		}
		
		return $this->model_classes;
	}
	
	//////////////////////////////////////////////////////
	//
	// Stages
	//
	//////////////////////////////////////////////////////
	
	/**
	 * @return TMm_WorkflowStage[]
	 */
	public function stages()
	{
		if($this->stages == null)
		{
			$this->stages = array();
			$query = "SELECT * FROM workflow_stages WHERE workflow_id = :id ORDER BY display_order ASC";
			$result = $this->DB_Prep_Exec($query, array('id' => $this->id()));
			while($row = $result->fetch())
			{
				$stage = TMm_WorkflowStage::init($row);
				$this->stages[$stage->id()] = $stage;
			}
		}
		
		return $this->stages;
	}
	
	public function createDefaultFirstStage()
	{
		// only bother if we don't have any yet
		if($this->numStages() == 0)
		{
			$values = array();
			$values['workflow_id'] = $this->id();
			$values['title'] = 'Start';
			$values['color'] = '333333';
			$values['display_order'] = 1;
			$stage = TMm_WorkflowStage::createWithValues($values);
			$this->stages[$stage->id()] = $stage;
		}
		
		
	}
	
	/**
	 * @return int
	 */
	public function numStages()
	{
		return count($this->stages());
	}
	
	/**
	 * Returns the first stage for this workflow
	 * @return TMm_WorkflowStage|bool
	 */
	public function firstStage()
	{
		$stages = array_values($this->stages());
		if(count($stages) > 0)
		{
			return $stages[0];
		}
		return false;
		
	}
	
	/**
	 * @param string $class_name
	 * @param int $class_id
	 */
	public function addModel($class_name, $class_id)
	{
		/** @var TCm_Model $model */
		$model = $class_name::init($class_id);
		
		if($model)
		{
			$this->addConsoleDebug('Adding Model');
			$this->addConsoleDebug('Class Name: '.$class_name);
			$this->addConsoleDebug('Class ID: '.$class_id);
			
			if($workflow_item = $this->workflowItemForModel($model))
			{
				// Existing workflow item
				$workflow_item->enable();
			}
			else
			{
				// No workflow item
				// create one
				$workflow_item = $this->createWorkflowItem($model);
			}
			
		}
		else
		{
			TC_message('Model with class "'.$class_name.'" not found');
		}
		
	}
	
	/**
	 * Returns the workflow item
	 * @param string|TCm_Model $model The name of a model or a model itself
	 * @param int|null $model_id
	 * @return bool|int|TMm_WorkflowItem
	 */
	public function createWorkflowItem($model, $model_id = null)
	{
		$values = array();
		if($model instanceof TCm_Model)
		{
			$values['model_name'] = $model->baseClassName();
			$values['model_id'] = $model->id();
		}
		else
		{
			$values['model_name'] = $model::baseClass();
			$values['model_id'] = $model_id;
		}
		$values['workflow_id'] = $this->id();
		return TMm_WorkflowItem::createWithValues($values);
	}
	
	/**
	 * Returns the workflow item associated with this item
	 * @param TCm_Model $model
	 * @return TMm_WorkflowItem|false
	 */
	public function workflowItemForModel($model)
	{
		$module = TMm_WorkflowModule::init();
		foreach($module->itemsForModel($model) as $workflow_item)
		{
			if($workflow_item->workflow()->id() == $this->id())
			{
				return $workflow_item;
			}
		}
		
		return false;
	}
	
	/**
	 * @param TMm_WorkflowStage $stage
	 * @return TMm_WorkflowItem[]
	 */
	public function workflowItemsForStage($stage)
	{
		if($this->workflow_items_by_stage == null)
		{
			$this->workflow_items_by_stage = array();
			
			$items = array();
			$module = TMm_WorkflowModule::init();
			$query = "SELECT * FROM workflow_items WHERE workflow_id = :workflow_id ORDER BY display_order ASC";
			$result = $this->DB_Prep_Exec($query, array('workflow_id'=> $this->id()));
			while($row = $result->fetch())
			{
				$workflow_item = TMm_WorkflowItem::init($row);
				if($workflow_item)
				{
					$current_stage = $workflow_item->currentStageModel();
					$this->workflow_items_by_stage[$current_stage->stage()->id()][] = $workflow_item;
				}
				
				
			}
		}
		
		if(!isset($this->workflow_items_by_stage[$stage->id()]))
		{
			return array();
		}
		
		
		return $this->workflow_items_by_stage[$stage->id()];
	}
	
}