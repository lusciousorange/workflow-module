<?php
class TMv_WorkflowItemCommentBox extends TCv_View
{
	protected $workflow_item;

	/**
	 * TMv_WorkflowItemCommentBox constructor.
	 *
	 * @param TMm_WorkflowItem $workflow_item
	 */
	public function __construct ($workflow_item)
	{
		$this->workflow_item = $workflow_item;
		// ID uses main workflow for matching
		parent::__construct('workflow_comments_box_'.$workflow_item->workflow()->id());
		$this->addClass('workflow_section_box');

		if($this->workflow_item->model())
		{
			$params = array(
				'model_name' => $this->workflow_item->model()->baseClassName(),
				'model_id' => $this->workflow_item->model()->id(),

			);
		}

		//$this->addClassCSSFile('TMv_WorkflowItemCommentBox');
		$this->addClassJQueryFile('TMv_WorkflowItemCommentBox');
		$this->addClassJQueryInit('TMv_WorkflowItemCommentBox', $params);



	}

	public function render ()
	{
		$heading = new TCv_View();
		$heading->setTag('h2');
		$heading->addText('Comments');
		$this->attachView($heading);

		$comment_form = new TCv_Form('workflow_comment_form_'.$this->workflow_item->workflow()->id());
		$comment_form->addClass('workflow_comment_form');
		$comment_form->addDataValue('workflow-id', $this->workflow_item->workflow()->id());
		$comment_form->setButtonText('Add Comment');

		$field = new TCv_FormItem_HTMLEditor('workflow_comment_'.$this->workflow_item->workflow()->id(),'Comment');
		$field->addClass('workflow_comment_box');
		$field->setTinyMCEInitPath($this->classFolderFromRoot().'/workflow_basic_comment_tinymce_init.js');

		$field ->setShowTitleColumn(false);

		$link_button = new TCv_Link('comment_link_button_'.$this->workflow_item->workflow()->id());
		$link_button->addClass('comment_link_button');
		$link_button->setIconClassName('fa-link');
		$link_button->addText('Comment Linked');
		$link_button->addClass('has_content_link');
		$link_button->setURL('#');
		$link_button->setTitle('Remove Link');
		$field->attachViewAfter($link_button);
		$comment_form->attachView($field);

		$field = new TCv_FormItem_Hidden('content_id_'.$this->workflow_item->workflow()->id(),'');
		$comment_form->attachView($field);

		$this->attachView($comment_form);

		$comment_page_title = new TCv_View('related_comments_title_'.$this->workflow_item->workflow()->id());
		$comment_page_title->addClass('comment_page_title');
		$comment_page_title->addText('Related Comments');
		$this->attachView($comment_page_title);
		

		$box = new TCv_View('workflow_model_comments_box_'.$this->workflow_item->workflow()->id());
		foreach($this->workflow_item->comments() as $comment)
		{
			$box->attachView(new TMv_WorkflowComment($comment)) ;
		}
		$this->attachView($box);
	}
}