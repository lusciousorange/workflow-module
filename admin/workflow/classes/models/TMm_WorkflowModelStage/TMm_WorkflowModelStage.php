<?php
class TMm_WorkflowModelStage extends TMm_WorkflowHistoryItem
{
	protected $stage_id;
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'match_id'; // [string] = The id column for this class
	public static $table_name = 'workflow_model_stages'; // [string] = The table for this class
	public static $model_title = 'Stage';
	public static $primary_table_sort = 'date_added DESC';
	
	public function __construct($id)
	{
		parent::__construct($id);
		
		// Validate that the stage still exists
		if(!$this->stage())
		{
			// No stage, this is an orphaned model stage, just delete it
			$this->delete();
			
			// Ensure the constructor fails
			$this->destroy();
			return false;
		}
	}


	/**
	 * Returns the  workflow stage
	 * @return bool|TMm_WorkflowStage
	 */
	public function stage()
	{
		return TMm_WorkflowStage::init($this->stage_id);
	}


	/**
	 * Move this item to a new stage, which means creating a new item with similar properties.
	 * @param int $id Not used
	 * @param int $id_2 The stage id
	 */
	public function moveToStage($id, $id_2)
	{
		// Create a new stage with the same values but
		$values = array();
		$values['stage_id'] = $id_2;
		$values['user_id'] = TC_currentUser()->id();
		$values['model_name'] = $this->modelName();
		$values['model_id'] = $this->modelID();
		
		TMm_WorkflowModelStage::createWithValues($values);

	}

	/**
	 * Returns the model for this workflow stage
	 * @return TCm_Model|TCu_Item
	 *
	 */
	public function model()
	{
		return ($this->modelName())::init($this->modelID());
	}

	/**
	 * Extends the functionality to also send slack messages if necessary
	 * @param array $values
	 * @param array $bind_param_values
	 * @param bool $return_new_model
	 * @return bool|TCm_Model
	 */
	public static function createWithValues ($values, $bind_param_values = array(), $return_new_model = true)
	{
		/** @var TMm_WorkflowModelStage $new_model_stage */
		$new_model_stage = parent::createWithValues($values, $bind_param_values,$return_new_model);

		// If it's the very first stage, don't send any notifications
		// Just get out
//		if(sizeof($new_model_stage->stages()) <= 1)
//		{
//			return $new_model_stage;
//		}

//		// Loop through and find out if user wants to receive the email
//		foreach($new_model_stage->people() as $user)
//		{
//			if($user->valueForProperty('workflow_email_stages'))
//			{
//				$email = new TMv_WorkflowstageEmail($new_model_stage, $user);
//				$email->send();
//			}
//
//		}


//		// HANDLE SLACK CALLS
//		// If Slack is installed
//		if(
//			TC_moduleWithNameInstalled('slack') && // Only bother if Slack is installed
//			class_exists('TMm_SlackClient') && // We also need the proper class
//			TC_getModuleConfig('workflow','slack_for_stages') // Respect system pref for Slack and Workflow
//			)
//		{
//			/** @var TMm_SlackClient $slack_client */
//			$slack_client = TC_initClass('TMm_SlackClient');
//
//			$model_title = $new_model_stage->model()->title();
//			$stage_title = $new_model_stage->stage()->title();
//
//
//			$title_text = $slack_client->cleanTextForSlack(TC_currentUser()->fullName()). " changed the status for " . $new_model_stage->typeTitle() ." : <".
//				$slack_client->fullDomainName().$new_model_stage->model()->adminEditURL()
//				."|".$slack_client->cleanTextForSlack($model_title).'>';
//
//			$slack_client->addText($title_text);
//
//			// Find list of people
//			$people_names = array();
//			foreach($new_model_stage->people() as $person)
//			{
//				$people_names[] = $slack_client->slackCodeForUser($person);
//			}
//
//			$attachment = array(
//				"fallback" => "",
//				"color" => '#'.$new_model_stage->stage()->color(),
//				"fields" => array(
//					array(
//						"title" => "Stage",
//						"value" => $slack_client->cleanTextForSlack($stage_title)
//					),
//					array(
//						"title" => "People",
//						"value" => implode(', ',$people_names)
//					)
//
//				)
//			);
//			$slack_client->addAttachment($attachment);
//			$slack_client->sendIncomingWebhook();
//
//
//		}
		return $new_model_stage;
	}


}
?>