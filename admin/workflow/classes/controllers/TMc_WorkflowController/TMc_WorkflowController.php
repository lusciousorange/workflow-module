<?php

/**
 * Class TMc_WorkflowController
 */
class TMc_WorkflowController extends TSc_ModuleController
{
	/**
	 * TSc_PagesController constructor.
	 * @param int|string|TSm_Module $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);
		
		$this->model = TC_website()->user();

	}

	/**
	 * Defines the URL targets
	 */
	public function defineURLTargets()
	{
		parent::defineURLTargets();
		
//		$target = TSm_ModuleURLTarget::init('');
//	//	$target->setNextURLTarget('manager');
//		$target->setViewName('TMv_WorkflowManager');
//		$this->addModuleURLTarget($target);
//
		
		$this->generateDefaultURLTargetsForModelClass(
			'TMm_WorkflowStage',
			'stage-',
			'TMm_Workflow',
			'workflow');
		
		$target = TSm_ModuleURLTarget::init('view');
		$target->setTitleUsingModelMethod('title');
		$target->setViewName('TMv_WorkflowKanban');
		$target->setModelName('TMm_Workflow');
		$target->setModelInstanceRequired();
		$target->setParentURLTargetWithName('');
		$this->addModuleURLTarget($target);
		
		$item_create = TSm_ModuleURLTarget::init( 'create');
		$item_create->setViewName('TMv_WorkflowForm');
		$item_create->setModelName('TMm_Workflow');
		$item_create->setTitle('New Workflow');
		$item_create->setParentURLTargetWithName('');
		$item_create->setAsRightButton('fa-plus');
		$this->addModuleURLTarget($item_create);
		
		$item = TSm_ModuleURLTarget::init( 'change-workflow-stage');
		$item->setModelName('TMm_WorkflowModelStage');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('moveToStage');
		$item->setTitleUsingModelMethod('title');
		$item->setPassValuesIntoActionMethodType('url');
		$item->setNextURLTarget('referrer');
		$this->addModuleURLTarget($item);

		$item = TSm_ModuleURLTarget::init( 'preview-comment-email');
		$item->setViewName('TMv_WorkflowPeopleEmail');
		$item->setModelName('TMm_WorkflowComment');
		$item->setModelInstanceRequired();
		$this->addModuleURLTarget($item);

		$item = TSm_ModuleURLTarget::init( 'preview-stage-email');
		$item->setViewName('TMv_WorkflowStageEmail');
		$item->setModelName('TMm_WorkflowModelStage');
		$item->setModelInstanceRequired();
		$this->addModuleURLTarget($item);

		$item = TSm_ModuleURLTarget::init( 'comment-toggle-resolved');
		$item->setModelName('TMm_WorkflowComment');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('toggleResolved');
		$item->setTitleUsingModelMethod('title');
		$item->setPassValuesIntoActionMethodType('url');
		$item->setNextURLTarget('referrer');
		$this->addModuleURLTarget($item);

		$item = TSm_ModuleURLTarget::init( 'comment-delete');
		$item->setModelName('TMm_WorkflowComment');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('delete');
		$item->setTitleUsingModelMethod('title');
		$item->setPassValuesIntoActionMethodType('url');
		$item->setNextURLTarget('referrer');
		$this->addModuleURLTarget($item);
		
		$item = TSm_ModuleURLTarget::init( 'add-workflow-to-model');
		$item->setModelName('TMm_Workflow');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('addModel');
		$item->setPassValuesIntoActionMethodType('url',2,3);
		$item->setNextURLTarget('referrer');
		$this->addModuleURLTarget($item);
		
		$item = TSm_ModuleURLTarget::init( 'disable-workflow-item');
		$item->setModelName('TMm_WorkflowItem');
		$item->setModelInstanceRequired();
		$item->setModelActionMethod('disable');
		$item->setNextURLTarget('referrer');
		$this->addModuleURLTarget($item);
		
		//////////////////////////////////////////////////////
		//
		// KANBAN
		//
		//////////////////////////////////////////////////////
		
		$item = TSm_ModuleURLTarget::init( 'kanban');
		$item->setViewName('TMv_WorkflowKanban');
		$item->setModelName('TMm_Workflow');
		$item->setModelInstanceRequired();
		$item->setTitle('Kanban');
		$this->addModuleURLTarget($item);

		$item = TSm_ModuleURLTarget::init( 'workflow-item-editor');
		$item->setViewName('TMv_WorkflowItemEditor');
		$item->setModelName('TMm_WorkflowItem');
		$item->setModelInstanceRequired();
		$item->setTitleUsingModelMethod('title');
		$this->addModuleURLTarget($item);



		//////////////////////////////////////////////////////
		//
		// SUBMENUS
		//
		//////////////////////////////////////////////////////
		$this->defineSubmenuGroupingWithURLTargets('edit','stage-list');//,'kanban');

	}
	


}
?>