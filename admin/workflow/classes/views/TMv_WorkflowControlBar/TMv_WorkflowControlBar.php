<?php

/**
 * Class TMv_WorkflowControlBar
 */
class TMv_WorkflowControlBar extends TCv_View
{
	/**
	 * TMv_WorkflowControlBar constructor.
	 * @param bool $id
	 */
	public function __construct ($id = false)
	{
		parent::__construct($id);

	}

	public function html ()
	{
		$module = TMm_WorkflowModule::init();
		if(! $module->workflowEnabled())
		{
			return '';
		}

		$params = array();
		if(isset($_GET['workflow-comments']))
		{
			$params['start_section'] = 'comments';
		}
		elseif(isset($_GET['workflow-people']))
		{
			$params['start_section'] = 'people';
		}
		elseif(isset($_GET['workflow-stages']))
		{
			$params['start_section'] = 'status';
		}


		$this->addClassCSSFile('TMv_WorkflowControlBar?v2');
		$this->addClassJQueryFile('TMv_WorkflowControlBar?v1');
		$this->addClassJQueryInit('TMv_WorkflowControlBar', $params);

		
		$model = $module->currentModel();
		$num_enabled_workflow_items = 0;
		if($model)
		{
			foreach($module->itemsForModel($model) as $workflow_item)
			{
				if($workflow_item->isEnabled())
				{
					$workflow = $workflow_item->workflow();
					$workflow_button = new TCv_Link('workflow_switch_button_' . $workflow->id());
					$workflow_button->setURL('#');
					$workflow_button->addText($workflow->code());
					$workflow_button->addClass('workflow_switch_button');
					$workflow_button->addDataValue('id', $workflow->id());
					$workflow_button->setTitle('Switch to ' . $workflow->title());
					$workflow_button->setAttribute('style', 'background-color:#' . $workflow->color() . ';');
					$workflow_button->addDataValue('code', $workflow->code());
					$workflow_button->addDataValue('title', $workflow->title());
					$workflow_button->addDataValue('color', $workflow->color());
					$workflow_button->addDataValue('color-light', $workflow->colorLight());
					$this->attachView($workflow_button);
					
					$num_enabled_workflow_items++;
				}
			}
		}

		if($num_enabled_workflow_items > 0)
		{
			
			$comment_button = new TCv_Link('workflow_control_status');
			//$comment_button->addText('Comments');
			$comment_button->setIconClassName('fa-list-ol');
			$comment_button->addClass('control_button');
			$comment_button->addClass('wf_button');
			$comment_button->addDataValue('section', 'status');
			$comment_button->setURL('#');
			$this->attachView($comment_button);
			
			$comment_button = new TCv_Link('workflow_control_comments');
			//$comment_button->addText('Comments');
			$comment_button->setIconClassName('fa-comments');
			$comment_button->addClass('control_button');
			$comment_button->addClass('wf_button');
			$comment_button->addDataValue('section', 'comments');
			$comment_button->setURL('#');
			$this->attachView($comment_button);
			
			$comment_button = new TCv_Link('workflow_control_people');
			//$comment_button->addText('People');
			$comment_button->setIconClassName('fa-users');
			$comment_button->addClass('control_button');
			$comment_button->addClass('wf_button');
			$comment_button->addDataValue('section', 'people');
			$comment_button->setURL('#');
			$this->attachView($comment_button);
		}
		if($module->currentUserIsSuper())
		{
			$comment_button = new TCv_Link('workflow_control_settings');
			$comment_button->addClass('control_button');
			$comment_button->setIconClassName('fa-cog');
			$comment_button->addDataValue('section', 'settings');
			$comment_button->addClass('');
			$comment_button->setURL('#');
			$this->attachView($comment_button);
		}
		
		$comment_button = new TCv_Link('workflow_control_toggle');
		$comment_button->addText(' ');
		$comment_button->setIconClassName('fa-chevron-left');
		$comment_button->setURL('#');
		$this->attachView($comment_button);
		
		
		$model = $module->currentModel();
		if($model)
		{
			$workflow_editor = TMv_WorkflowEditor::init($model);
			$this->attachView($workflow_editor);
			
		}
		
	

//		$collab_icon = new TCv_View();
//		$collab_icon->setTag('span');
//		//$collab_icon->addClass('workflow_bar_icon');
//		//$collab_icon->addClass('fa-handshake-o');
//		$collab_icon->addText('Workflow');
//		$this->attachView($collab_icon);

//		$collab_text = new TCv_View();
//		$collab_text->setTag('span');
//		$collab_text->addText('Workflow');
//		$this->attachView($collab_text);




		return parent::html();
	}

}