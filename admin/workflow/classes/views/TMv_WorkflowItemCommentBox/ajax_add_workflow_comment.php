<?php
	require($_SERVER['DOCUMENT_ROOT'] . "/admin/system/headers/tungsten_header.php");


	$values = array();
	$values['model_name'] = $_POST['model_name'];
	$values['model_id'] = $_POST['model_id'];
	$values['comment'] = $_POST['comment'];
	$values['workflow_id'] = $_POST['workflow_id'];
	$values['user_id'] = TC_currentUser()->id();
	if($_POST['content_id'] != '')
	{
		$values['content_id'] = $_POST['content_id'];
	}

	$comment = TMm_WorkflowComment::createWithValues($values);

	if($comment)
	{
		$comment_view = new TMv_WorkflowComment($comment);
		print  $comment_view->html();

	}


?>