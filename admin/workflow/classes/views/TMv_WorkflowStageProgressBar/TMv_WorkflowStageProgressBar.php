<?php
class TMv_WorkflowStageProgressBar extends TCv_View
{
	protected $workflow_module;
	
	/** @var TMm_WorkflowStage $workflow_stage  */
	protected $workflow_stage;
	
	protected $check_svg = '';
	
	/**
	 * TMv_WorkflowStageProgressBar constructor.
	 * @param TMm_WorkflowStage $workflow_stage
	 */
	public function __construct($workflow_stage)
	{
		$this->workflow_module = TMm_WorkflowModule::init();
		
		$this->workflow_stage = $workflow_stage;
		parent::__construct('workflow_progress_bar'.$workflow_stage->id());
		
		$this->addClassCSSFile('TMv_WorkflowStageProgressBar');
		
		
		$svg_code = file_get_contents($_SERVER['DOCUMENT_ROOT'].$this->classFolderFromRoot().'/check.svg');
		$this->check_svg = $svg_code;
		
	}
	
	public function render()
	{
		$done = true;
		foreach ($this->workflow_stage->workflow()->stages() as $stage)
		{
			$dot = new TCv_View();
		//	$dot->setAttribute('title',$stage->title());
			$dot->addClass('stage_dot');
			$dot->setAttribute('style',
			                   'background-color:#'.$stage->color().';'.
			                         'color:#'.$stage->readableColor().';'.
			                         'border-color:#'.$stage->color().';');
			
			if($stage->id() == $this->workflow_stage->id())
			{
				$dot->addClass('current_stage');
				$done = false; // those after this are not done
			}
			
			$inside = new TCv_View();
			$inside->addClass('inside');
			$inside->setAttribute('style','background-color:#'.$stage->color().';');
			$dot->attachView($inside);
			
			
			
			if($done)
			{
				$dot->addText($this->check_svg);
				$dot->addClass('done');
			}
			
			$dot->addDataValue('title',$stage->title());
			
			$this->attachView($dot);
		}
	}
}