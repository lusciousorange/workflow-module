(function ($)
{
// INIT
	$.fn.TMv_WorkflowEditor = function(options)
	{
		var settings = $.extend(
			{
				model_name : false,
				model_id : false,
				default_stage_text : {},
				default_stage_style : {},
			}, options || {});






		function stageDotMouseOver(e)
		{
			var target = $(e.currentTarget);
			var stage_box = target.closest('.workflow_section_box').find('.stage_box');
			stage_box.html(target.attr('data-title')).attr('style',target.attr('style'));
		}

		function stageDotMouseOut(e)
		{
			var target = $(e.currentTarget);

			var section_box = target.closest('.workflow_section_box');
			var workflow_id = section_box.attr('data-workflow-id');
			var stage_box = section_box.find('.stage_box');

			stage_box
				.html(settings.default_stage_text[workflow_id])
				.attr('style', settings.default_stage_style[workflow_id]);
		}


		/**********************************************/
		/*										   	  */
		/*                MAIN RETURN                 */
		/*										   	  */
		/**********************************************/

		return this.each(function()
		{
			// add listeners


			var editor = $('.TMv_WorkflowEditor');


			editor.on( "mouseenter", ".stage_dot", function(event) { stageDotMouseOver(event);  });
			editor.on( "mouseleave", ".stage_dot", function(event) { stageDotMouseOut(event);  });

			$('.TMv_WorkflowEditor .stage_box').each(function(id,box){
				var workflow_id = $(box).attr('data-workflow-id');
				settings.default_stage_text[workflow_id] = $('#workflow_status_box_'+workflow_id+' .stage_dot.current_stage').attr('data-title');
				settings.default_stage_style[workflow_id] = $(box).attr('style');

			});




		});


	};


// END THE WRAPPER
})(jQuery);
