<?php
class TMv_WorkflowForm extends TCv_FormWithModel
{
	/**
	 * TMv_WorkflowStageForm constructor.
	 * @param string|TMm_Workflow $model
	 */
	public function __construct($model)
	{
		parent::__construct($model);
		
		$this->setSuccessURL('');
		
	}

	

	public function configureFormElements()
	{

		
		$field = new TCv_FormItem_TextField('title', 'Title');
		$field->setIsRequired();
		$this->attachView($field);
		
		$field = new TCv_FormItem_TextField('code', 'Code');
		$field->addHelpText('The short code for this, no spaces, 3 letters');
		$field->setIsRequired();
		$this->attachView($field);
		
		$field = new TCv_FormItem_TextField('model_class_names', 'Model Class Names');
		$field->setHelpText('The comma-separated list of PHP model class names that that of this website will use the workflow .');
		$this->attachView($field);
		
		$field = new TCv_FormItem_TextField('color', 'Color');
		$field->setIsRequired();
		$field->setHelpText('The 6-digit hex color for this workflow. Must be a darker color that works with white text.');
		$this->attachView($field);




//		$field = new TCv_FormItem_TextBox('description', 'Description');
//		$this->attachView($field);

//		$user_group_list = new TMm_UserGroupList();
//		if(sizeof($user_group_list->models()) > 1)
//		{
//			$user_groups = new TCv_FormItem_CheckboxList('user_groups', 'Exit Control Groups');
//			$user_groups->setHelpText("Indicate the groups who can move this item to a different stage in the workflow. Only users in this user group can move this item to another status. ");
//			$user_groups->setSaveToDatabase(false);
//
//			$all_user_groups = $user_group_list->groups();
//			unset($all_user_groups[1]); // remove system admin
//			$selected_groups = array();
//			if($this->isEditor())
//			{
//				$selected_groups = $this->model()->userGroups();
//			}
//			$user_groups->setValuesFromObjectArrays($all_user_groups, $selected_groups);
//			$user_groups->addText('<p>Note: System Administrators have full workflow control.</p>');
//			$this->attachView($user_groups);
//		}

//		$stage_list = new TMm_WorkflowStageList();
//		$next_stages = new TCv_FormItem_CheckboxList('next_stages', 'Possible Next Stages');
//		$next_stages->setHelpText("Indicate the next stages that this stage can transition to. ");
//		$next_stages->setSaveToDatabase(false);
//
//		$all_next_stages = $stage_list->models();
//		$selected_stages = array();
//
//		if($this->isEditor())
//		{
//			unset($all_next_stages[$this->model()->id()]); // remove system admin
//			$selected_stages = $this->model()->nextStages();
//		}
//		$next_stages->setValuesFromObjectArrays($all_next_stages, $selected_stages);
//		$this->attachView($next_stages);


	}
	
	/**
	 * @param TCc_FormProcessorWithModel $form_processor
	 */
	public static function customFormProcessor_Validation($form_processor)
	{
		$hex = $form_processor->formValue('color');
		$hex = strtoupper(str_ireplace('#', '', $hex));
		$form_processor->addDatabaseValue('color', $hex );
		
		if(!preg_match('/^[0-9A-F]{6}$/i',$hex))
			//if(strlen($hex) != 6)
		{
			$form_processor->failFormItemWithID('color', 'Color must be exactly 6 HEX digits such as A5B2C3');
		}
		
		
		
		
	}


}