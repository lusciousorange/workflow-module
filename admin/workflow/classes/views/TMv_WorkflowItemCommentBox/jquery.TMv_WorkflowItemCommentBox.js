(function ($)
{
// INIT
	$.fn.TMv_WorkflowItemCommentBox = function(options)
	{

		var settings = $.extend(
			{
				model_name : false,
				model_id : false,
			}, options || {});



		function deleteCommentClicked(event)
		{
			event.preventDefault();

			var comment_id = $(event.currentTarget).attr('data-comment-id');
			var url_data = new Array();
			url_data.push('cancel_next_action=1');

			jQuery.ajax(
				{
					url: "/admin/workflow/do/comment-delete/" + encodeURI(comment_id),
					type: "GET",
					data: url_data.join('&'),
					success: function(data)
					{
						$('#TMm_WorkflowComment--' + comment_id).remove();
					}

				});



		}


		function resolveCommentClicked(event)
		{
			event.preventDefault();

			var comment_id = $(event.currentTarget).attr('data-comment-id');
			var url_data = new Array();
			url_data.push('cancel_next_action=1');

			jQuery.ajax(
				{
					url: "/admin/workflow/do/comment-toggle-resolved/" + encodeURI(comment_id),
					type: "GET",
					data: url_data.join('&'),
					success: function(data)
					{
						$('#TMm_WorkflowComment--' + comment_id).toggleClass('resolved');
					}

				});



		}

		function removeCommentLinkClicked(event)
		{
			event.preventDefault();

			disableCommentLinkButton();



		}

		function disableCommentLinkButton()
		{
			$('.comment_link_button').attr('data-highlight-target','').slideUp();
		}


		function commentSubmitted(event)
		{
			event.preventDefault();

			var workflow_id = $(event.target).attr('data-workflow-id');
			var url_data = [];
			url_data.push('model_name'+ '=' + encodeURI(settings.model_name));
			url_data.push('model_id'+ '=' + encodeURI(settings.model_id));
			url_data.push('comment'+ '=' + encodeURIComponent( window.tinyMCE.get('workflow_comment_'+workflow_id).getContent() ));
			url_data.push('content_id'+ '=' + encodeURIComponent( $('#workflow_comment_form_'+workflow_id+' #content_id_'+workflow_id).val()));
			url_data.push('workflow_id'+ '=' + encodeURIComponent(workflow_id));

			jQuery.ajax(
				{
					url: "/admin/workflow/classes/views/TMv_WorkflowItemCommentBox/ajax_add_workflow_comment.php",
					type: "POST",
					data: url_data.join('&'),
					success: function(data)
					{
						// Add the content box
						$('#workflow_model_comments_box_'+workflow_id).prepend(data);

						// Reset the Form
						$('#workflow_comment_form_'+workflow_id+' #content_id_'+workflow_id).val('');
						disableCommentLinkButton();

						// Empty the editor
						window.tinyMCE.get('workflow_comment_'+workflow_id).setContent('');

						$('.workflow_comment_form').removeClass('tungsten_loading');
					}

				});


		}


		return this.each(function()
		{
			$('.workflow_comment_form').submit(function(event) { commentSubmitted(event)});

			var editor = $('.TMv_WorkflowItemCommentBox');
			editor.on( "click", ".comment_delete", function(event) { deleteCommentClicked(event);  });
			editor.on( "click", ".comment_toggle_resolved", function(event) { resolveCommentClicked(event);  });
			editor.on( "click", ".comment_link_button", function(event) { removeCommentLinkClicked(event);  });

			// Track clicks on the content boxes
			$(".comment_link_button").hide();


		});


};


// END THE WRAPPER
})(jQuery);
