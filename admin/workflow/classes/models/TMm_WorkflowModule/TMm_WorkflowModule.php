<?php
class TMm_WorkflowModule extends TSm_Module
{
	protected $workflow_enabled = null;
	protected $workflow_model;
	protected $model_stages = array();
	protected $workflow_items = array();
	protected $model_comments = array();
	protected $model_users = array();
	protected $stages = array();

	protected $workflows = array();
	
	/**
	 * TMm_WorkflowModule constructor.
	 * @param array|int|bool $module_id
	 */
	public function __construct($module_id = false)
	{
		if($module_id === false)
		{
			$module_id = 'workflow';
		}
		parent::__construct($module_id);
		
		$this->setAsAutoUpdate();
	}
	
	public function runAfterInit()
	{
		parent::runAfterInit();
		
		$this->workflow_enabled = $this->installed() && TC_getModuleConfig('workflow', 'workflow_enabled');
		
		// Stop if we're not enabled
		if(!$this->workflowEnabled())
		{
			return;
		}
		
		// Don't do anything if an upgrade is required
		if($this->requiresUpgrade())
		{
			return;
		}
		
		
		// ----- WORKFLOWS -----
		$query = "SELECT * FROM workflows ORDER BY workflow_id ASC ";
		$result = $this->DB_Prep_Exec($query);
		while($row = $result->fetch())
		{
			$this->workflows[] = TMm_Workflow::init($row);
			
			// TODO : create first stage if none exist
		}
		
		// No workflows, create the first one
		if(sizeof($this->workflows) == 0)
		{
			$workflow_values = array(
				'workflow_id' => 1,
				'title'=>'Content Strategy',
				'code' => 'CS',
				'color' => '630063',
				'model_class_names' => 'TMm_PagesMenuItem');
			$workflow = TMm_Workflow::createWithValues($workflow_values);
			$this->workflows[] = $workflow;
		}
		
		foreach($this->workflows as $workflow)
		{
			if($workflow->numStages() == 0)
			{
				$workflow->createDefaultFirstStage();
			}
		}
		
		$current_url_target = TC_currentURLTarget();
		
		// Special Case for Page Menu Manager
		if($current_url_target && $current_url_target->viewName() == 'TMv_PagesMenuManager')
		{
			$this->workflow_model = false;
			$this->initAllModels('TMm_PagesMenuItem');
		}
		
		// Find active model OR detect a list with no active model
		foreach (TC_activeModels() as $active_model)
		{
			foreach($this->workflows as $workflow)
			{
				foreach($workflow->modelClassNames() as $model_name)
				{
					// We only need to find one once
					if(is_null($this->workflow_model))
					{
						// Check if we have an active model that matches exact
						// THis means we're viewing a thing that has workflow
						if($active_model instanceof $model_name)
						{
							$this->workflow_model = $active_model;
						}
						
					}
				}
			}
		}
		
	}
	
	/**
	 * @return TCm_Model
	 */
	public function currentModel()
	{
		return $this->workflow_model;
	}
	
	/**
	 * Initializes all the models with a given class name
	 * @param string $class_name
	 */
	protected function initAllModels($class_name)
	{
		// Init the array
		$this->model_stages[$class_name] = array();
		
		// Try to mass instantiate the items
		$list_class_name = $class_name.'List';
		
		// Deal with inconsistent naming in pages module
		if($class_name == 'TMm_PagesMenuItem')
		{
			$list_class_name = 'TMm_PagesMenusList';
		}
		if(class_exists($list_class_name))
		{
			$model_list = $list_class_name::init();
			if($model_list instanceof TCm_ModelList)
			{
				$model_list->models();
			}
			
		}
		
		// Find the all
		$query = "SELECT * FROM workflow_items WHERE model_name = :class_name ORDER BY date_added DESC";
		$result = $this->DB_Prep_Exec($query, array('class_name' => $class_name));
		while($row = $result->fetch())
		{
			$item = TMm_WorkflowItem::init($row);
			if($item)
			{
				// Store an array of stages for the model
				if(!isset($this->workflow_items[$class_name][$item->modelID()]))
				{
					$this->workflow_items[$class_name][$item->modelID()] = array();
				}
				
				$this->workflow_items[$class_name][$item->modelID()][$item->workflow()->id()] = $item;
			}
		}
		
		
		
		// Find the all
		$query = "SELECT * FROM workflow_model_stages WHERE model_name = :class_name ORDER BY date_added DESC";
		$result = $this->DB_Prep_Exec($query, array('class_name' => $class_name));
		while($row = $result->fetch())
		{
			$model_stage = TMm_WorkflowModelStage::init($row);
			if($model_stage)
			{
				// Store an array of stages for the model
				if(!isset($this->model_stages[$class_name][$model_stage->modelID()]))
				{
					$this->model_stages[$class_name][$model_stage->modelID()] = array();
				}
				
				$this->model_stages[$class_name][$model_stage->modelID()][] = $model_stage;
			}
		}
		
		$query = "SELECT * FROM workflow_model_comments WHERE model_name = :class_name ORDER BY date_added DESC";
		$result = $this->DB_Prep_Exec($query, array('class_name' => $class_name));
		while($row = $result->fetch())
		{
			$model_id = $row['model_id'];
			$comment = TMm_WorkflowComment::init($row);
			// Store an array of stages for the model
			if(!isset($this->model_comments[$class_name][$model_id]))
			{
				$this->model_comments[$class_name][$model_id] = array();
			}
			
			$this->model_comments[$class_name][$model_id][] = $comment;
			
		}
		
		$query = "SELECT * FROM workflow_model_users WHERE model_name = :class_name ORDER BY date_added DESC";
		$result = $this->DB_Prep_Exec($query, array('class_name' => $class_name));
		while($row = $result->fetch())
		{
			$model_id = $row['model_id'];
			$comment = TMm_WorkflowUser::init($row);
			// Store an array of stages for the model
			if(!isset($this->model_users[$class_name][$model_id]))
			{
				$this->model_users[$class_name][$model_id] = array();
			}
			
			$this->model_users[$class_name][$model_id][] = $comment;
			
		}
		
		
		
	}
	
	/**
	 * @param TCm_Model $model
	 * @return bool
	 */
	public function hasStagesForModel($model)
	{
		return isset($this->model_stages [$model->baseClassName()] [$model->id()] );
	}
	
	/**
	 * @param TCm_Model $model
	 * @return TMm_WorkflowModelStage[]
	 */
	public function stagesForModel($model)
	{
		if(!$this->hasStagesForModel($model))
		{
			$this->model_stages[$model->baseClassName()][$model->id()] = array();
			
			$query = "SELECT * FROM workflow_model_stages WHERE model_name = :model_name AND model_id = :model_id  ORDER BY date_added DESC";
			$result = $this->DB_Prep_Exec($query,
			                              array('model_name' => $model->baseClassName(),
				                              'model_id' => $model->id()));
			while($row = $result->fetch())
			{
				$stage_model = TMm_WorkflowModelStage::init($row);
				if($stage_model)
				{
					$this->model_stages[$model->baseClassName()][$model->id()][$stage_model->id()] = $stage_model;
				}
				
			}
			
		}
		
		return $this->model_stages [$model->baseClassName()] [$model->id()];
	}
	
	/**
	 * @return TMm_Workflow[]
	 */
	public function workflows()
	{
		return $this->workflows;
	}
	
	/**
	 * Returns if the provided model object or model name is workflow enabled
	 * @param string|TCm_Model $model_class
	 * @return bool
	 */
	public function modelClassIsWorkflowEnabled($model_class)
	{
		foreach($this->workflows() as $workflow)
		{
			foreach($workflow->modelClassNames() as $class_name)
			{
				if($model_class instanceof $class_name)
				{
					return true;
				}
				elseif(is_string($model_class) && $model_class == $class_name)
				{
					return true;
				}
			}
		}
		return false;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// ITEMS
	//
	//////////////////////////////////////////////////////
	
	/**
	 * @param TCm_Model $model
	 * @return bool
	 */
	public function hasItemsForModel($model)
	{
		return isset($this->workflow_items [$model->baseClassName()] [$model->id()] );
	}
	
	/**
	 * @param TCm_Model $model
	 * @return TMm_WorkflowItem[]
	 */
	public function itemsForModel($model)
	{
		$base_class_name = $model->baseClassName();
		
		if(!$this->hasItemsForModel($model))
		{
			$this->workflow_items[$base_class_name][$model->id()] = array();
			
			$query = "SELECT * FROM workflow_items WHERE model_name = :model_name AND model_id = :model_id  ORDER BY date_added DESC";
			$result = $this->DB_Prep_Exec($query,
			                              array('model_name' => $base_class_name,
				                              'model_id' => $model->id()));
			while($row = $result->fetch())
			{
				$item = TMm_WorkflowItem::init($row);
				if($item)
				{
					// index by class name, then ID, then workflow ID
					$this->workflow_items[$base_class_name][$model->id()][$item->workflow()->id()] = $item;
				}
			}
			
		}
		
		// Check for required items that don't exist
		$this->generateRequiredItemsForModel($model);
		
		// Return all the items we have for this model
		return $this->workflow_items [$base_class_name] [$model->id()];
	}
	
	/**
	 * @param TCm_Model $model
	 */
	protected function generateRequiredItemsForModel($model)
	{
		foreach($this->workflows() as $workflow)
		{
			foreach($workflow->modelClassNames() as $class_name)
			{
				// Only generate if the provide model matches one of the class names
				if($model instanceof $class_name)
				{
					// if the item does not exist for the class name, this is the real problem
					if(!isset($this->workflow_items[$class_name][$model->id()][$workflow->id()]))
					{
						$item = $workflow->createWorkflowItem($model);
						
						// Save it to our cache
						$this->workflow_items[$class_name][$model->id()][$workflow->id()] = $item;
						
						
					}
				}
				
				
			}
		}
	}
	
	/**
	 * Creates the first item for the given workflow item. This only has an effect if the workflow doesn't have any stages
	 * as of yet. This method exists within the Module, so that it can handle system-wide caching properly.
	 * @param TMm_WorkflowItem $workflow_item
	 * @return TMm_WorkflowModelStage
	 */
	public function createFirstModelStageForWorkflowItem($workflow_item)
	{
		$workflow = $workflow_item->workflow();
		
		// Generate the first stage if missing
		$class_name = $workflow_item->modelName();
		$model_id = $workflow_item->modelID();
		
		$stage = $workflow->firstStage();
		// Create the first stage
		$values = array();
		$values['stage_id'] = $stage->id();
		$values['model_name'] = $class_name;
		$values['model_id'] = $model_id;
		$values['user_id'] = TC_currentUser()->id();
		
		$model_stage = TMm_WorkflowModelStage::createWithValues($values);
		
		// Deal with cache
		$this->model_stages[$class_name][$model_id][$model_stage->id()] = $model_stage;
			
		return $model_stage;
	}
	
	/**
	 * Returns if the provided model uses a workflow
	 * @param TCm_Model $model
	 * @param TMm_Workflow $workflow
	 * @return TMm_WorkflowItem|bool
	 */
	public function workflowItemForModelAndWorkflow($model, $workflow)
	{
		foreach($this->itemsForModel($model) as $workflow_item)
		{
			if($workflow_item->workflow()->id() == $workflow->id())
			{
				return $workflow_item;
			}
		}
		return false;
	}

	
	//////////////////////////////////////////////////////
	//
	// COMMENTS
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * @param TCm_Model $model
	 * @return bool
	 */
	public function hasCommentsForModel($model)
	{
		return isset($this->model_comments [$model->baseClassName()] [$model->id()] );
	}
	
	/**
	 * @param TCm_Model $model
	 * @return int
	 */
	public function numCommentsForModel($model)
	{
		return count($this->commentsForModel($model));
	}
	
	/**
	 * @param TCm_Model $model
	 * @return TMm_WorkflowComment[]
	 */
	public function commentsForModel($model)
	{
		if($this->hasCommentsForModel($model))
		{
			return $this->model_comments [$model->baseClassName()] [$model->id()];
		}
		elseif(!isset($this->model_comments [$model->baseClassName()])) // we don't anything for this class
		{
			$this->model_comments[$model->baseClassName()][$model->id()] = array();
			
			$query = "SELECT * FROM workflow_model_comments WHERE model_name = :model_name AND model_id = :model_id  ORDER BY date_added DESC";
			$result = $this->DB_Prep_Exec($query,
			                              array('model_name' => $model->baseClassName(),
				                              'model_id' => $model->id()));
			while($row = $result->fetch())
			{
				$model_id = $model->id();
				$comment = TMm_WorkflowComment::init($row);
				
				
				$this->model_comments[$model->baseClassName()][$model->id()][] = $comment;
				
			}
			
			// Return what we just found
			return $this->model_comments [$model->baseClassName()] [$model->id()];
		}
		
		// Return empty array
		return array();
	}
	
	//////////////////////////////////////////////////////
	//
	// USERS
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * @param TCm_Model $model
	 * @return bool
	 */
	public function hasUsersForModel($model)
	{
		return isset($this->model_users [$model->baseClassName()] [$model->id()] );
	}
	
	/**
	 * @param TCm_Model $model
	 * @return int
	 */
	public function numUsersForModel($model)
	{
		return count($this->usersForModel($model));
	}
	
	/**
	 * @param TCm_Model $model
	 * @return TMm_WorkflowUser[]
	 */
	public function usersForModel($model)
	{
		if(!$this->hasUsersForModel($model))
		{
			$this->model_users[$model->baseClassName()][$model->id()] = array();
			
			$query = "SELECT * FROM workflow_model_users WHERE model_name = :model_name AND model_id = :model_id  ORDER BY date_added DESC";
			$result = $this->DB_Prep_Exec($query,
			                              array('model_name' => $model->baseClassName(),
				                              'model_id' => $model->id()));
			while($row = $result->fetch())
			{
				$model_id = $model->id();
				$user = TMm_WorkflowUser::init($row);
				
				
				$this->model_users[$model->baseClassName()][$model->id()][] = $user;
				
			}
		}
		
		return $this->model_users [$model->baseClassName()] [$model->id()];
	}

	/**
	 * Returns if the provided model uses a workflow
	 * @param TCm_Model $model
	 * @param TMm_Workflow $workflow
	 * @return TMm_WorkflowUser|bool
	 */
	public function workflowUserForModelAndWorkflow($model, $workflow)
	{
		foreach($this->usersForModel($model) as $workflow_user)
		{
			if($workflow_user->workflow()->id() == $workflow->id())
			{
				return $workflow_user;
			}
		}
		return false;
	}


	/**
	 * Returns if the current user is a super user
	 * @return bool
	 */
	public function currentUserIsSuper()
	{
		$user = TC_currentUser();
		$super_user_id_string = TC_getModuleConfig('workflow', 'superuser_ids');
		$super_user_ids = explode(',', $super_user_id_string);
		foreach($super_user_ids as $id)
		{
			if($user->id() == $id)
			{
				return true;
			}
			
		}
		
		return false;
	}
	
	
	//////////////////////////////////////////////////////
	//
	// GENERAL SETTINGS
	//
	//////////////////////////////////////////////////////
	
	
	/**
	 * Returns if workflow is currently enabled
	 * @return bool
	 */
	public function workflowEnabled()
	{
		return $this->workflow_enabled;
	}
	
}