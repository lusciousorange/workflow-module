<?php
class TMv_WorkflowList extends TCv_ModelList
{
	public function __construct()
	{
		parent::__construct();
		
		$this->setModelClass('TMm_Workflow');
		$this->defineColumns();
		
		$this->addClassCSSFile();
	}
	
	public function defineColumns()
	{
		$column = new TCv_ListColumn('title');
		$column->setTitle('Title');
		$column->setContentUsingListMethod('titleColumn');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('code');
		$column->setTitle('Code');
		$column->setContentUsingModelMethod('code');
		$this->addTCListColumn($column);
		
		$column = new TCv_ListColumn('color');
		$column->setTitle('Color');
		$column->setContentUsingListMethod('colorColumn');
		$this->addTCListColumn($column);
		
		
		
		$column = new TCv_ListColumn('stages');
		$column->setTitle('Stages');
		$column->setContentUsingListMethod('stagesColumn');
		$this->addTCListColumn($column);
	
		$column = new TCv_ListColumn('Models');
		$column->setTitle('Models');
		$column->setContentUsingListMethod('modelsColumn');
		$this->addTCListColumn($column);
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Workflow $model
	 * @return TCv_View|string
	 */
	public function titleColumn($model)
	{
		$link = $this->linkForModuleURLTargetName($model, 'edit');
		$link->addText($model->title());
		return $link;
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Workflow $model
	 * @return TCv_View|string
	 */
	public function colorColumn($model)
	{
		$view = new TCv_View();
		$view->setTag('span');
		$view->addClass('color_box');
		$view->setAttribute('style','background-color:#'.$model->color().';color:#FFF; padding:5px 8px;');
		
		$view->addText('#' . $model->color());
		
		return $view;
		
	}
	
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Workflow $model
	 * @return TCv_View|string
	 */
	public function stagesColumn($model)
	{
		//$link = $this->controlButton($model,'stage-list');
		
		$link = $this->linkForModuleURLTargetName($model, 'stage-list');
		$link->addText($model->numStages(). ' Stages');
		return $link;
	}
	
	/**
	 * Returns the column value for the provided model
	 * @param TMm_Workflow $model
	 * @return TCv_View|string
	 */
	public function modelsColumn($model)
	{
		//$link = $this->controlButton($model,'stage-list');
		
		return implode('<br />', $model->modelClassNames());
	}
	
}