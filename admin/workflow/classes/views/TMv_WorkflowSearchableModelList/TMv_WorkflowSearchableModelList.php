<?php

/**
 * Class TMv_WorkflowSearchableModelList
 *
 * @deprecated : Functionality built into TCv_SearchableModelList and TCv_ModelList
 */
class TMv_WorkflowSearchableModelList extends TCv_SearchableModelList
{
	protected $show_workflow = false;

	/**
	 * TMv_WorkflowSearchableModelList constructor.
	 */
	public function __construct()
	{
		$this->show_workflow = TC_moduleWithNameInstalled('workflow') && TC_getModuleConfig('workflow', 'workflow_enabled');
		parent::__construct();

	}

	/**
	 * Adds the workflow column to the list. THis validates that the column should even be shown
	 */
	public function addWorkflowColumn()
	{
		if($this->show_workflow )
		{
			$column = new TCv_ListColumn('workflow');
			$column->setTitle('Workflow');
			$column->setContentUsingListMethod('workflowColumn');
			$this->addTCListColumn($column);
		}
	}

	/**
	 * Extends the functionality of html() to add the workflow column to the start
	 */
	public function html()
	{
		$this->addWorkflowColumn();
		return parent::html();
	}

	/**
	 * Processes the values provided via the ajax searching script and returns the values to be returned
	 * @param TCm_ModelList $model_list
	 * @param array $params
	 *
	 * @return array
	 */
	public function processFilterValues($model_list, $params)
	{
		$this->addWorkflowColumn();
		return parent::processFilterValues($model_list, $params);

	}


		/**
	 * Adds in the filtering of the stages
	 *
	 * @param TCm_Model[] $objects
	 * @param array $params
	 * @return TCm_Model[]
	 */
	protected function postFilterObjects($objects, $params)
	{
		if(!$this->show_workflow)
		{
			return $objects;
		}

		if($params['workflow_stage_id'] != '')
		{
			/** @var TMm_WorkflowModelStageList $workflow_model_stage_list */
			$workflow_model_stage_list = TC_initClass('TMm_WorkflowModelStageList');
			$stages = $workflow_model_stage_list->currentStagesForModelName($this->model_class);
			foreach($objects as $index => $model)
			{
				if($params['workflow_stage_id'] != $stages[$model->id()]->id())
				{
					unset($objects[$index]);
				}

			}
		}

		if($params['workflow_user_id'] != '')
		{
			/** @var TMm_WorkflowUserList $workflow_model_stage_list */
			$workflow_model_stage_list = TC_initClass('TMm_WorkflowUserList');
			$workflow_users = $workflow_model_stage_list->currentUsersForModelName($this->model_class);
			foreach($objects as $index => $model)
			{

				// No Users for this item
				if(!isset($workflow_users[$model->id()][$params['workflow_user_id']]) )
				{
					unset($objects[$index]);
				}
			}
		}

		return $objects;
	}


	/**
	 * The column for the workflow for the product
	 * @param TCm_Model $model
	 * @return TMv_WorkflowSummary
	 */
	public function workflowColumn($model)
	{
		return new TMv_WorkflowSummary($model);
	}




	public function defineFilters()
	{
		parent::defineFilters();

		if($this->show_workflow )
		{
			$list = TC_initClass('TMm_WorkflowStageList');
			$field = new TCv_FormItem_Select('workflow_stage_id', 'Workflow Stage');
			$field->addOption('', 'All Stages');
			foreach($list->models() as $stage)
			{
				$field->addOption($stage->id(), $stage->title());
			}

			$this->addFilterFormItem($field);

			$list = TC_initClass('TMm_UserList');
			$field = new TCv_FormItem_Select('workflow_user_id', 'Workflow Users');
			$field->addOption('', 'All Users');
			foreach($list->models() as $user)
			{
				$field->addOption($user->id(), $user->title());
			}

			$this->addFilterFormItem($field);

		}
	}


}
?>