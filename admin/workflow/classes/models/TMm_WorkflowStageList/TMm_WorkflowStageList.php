<?php
class TMm_WorkflowStageList extends TCm_ModelList
{

	public function __construct($init_model_list = true)
	{
		parent::__construct('TMm_WorkflowStage',$init_model_list);
		
	}

	/**
	 * @param bool $subset_name
	 * @return TMm_WorkflowStage[]
	 */
	public function models($subset_name = false)
	{
		return parent::models($subset_name);
	}

	public function numStages()
	{
		return sizeof($this->models());
	}

	/**
	 * Returns the first stage available
	 * @return bool | TMm_WorkflowStage
	 */
	public function firstStage()
	{
		foreach($this->models() as $model)
		{
			return $model;
		}

		return false;
	}


}

?>