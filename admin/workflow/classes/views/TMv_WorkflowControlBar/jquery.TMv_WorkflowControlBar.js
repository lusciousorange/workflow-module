(function ($)
{
// INIT
	$.fn.TMv_WorkflowControlBar = function(options)
	{
		var settings = $.extend(
			{
				model_name : false,
				scroll_timer : false,
				start_section : false
			}, options || {});

		function toggleClicked(event)
		{
			if(typeof(event) == 'object' )
			{
				event.preventDefault();
			}

			$('body').toggleClass('showing_workflow');
			$('#workflow_control_toggle').toggleClass('hiding_workflow');

			sessionStorage.setItem('workflow_show', !JSON.parse(sessionStorage.getItem('workflow_show')));

		}

		/**
		 * Forces the workflow panel to show
		 */
		function showPanel()
		{
			$('.TMv_WorkflowEditor').show();
			$('body').addClass('showing_workflow');
			$('#workflow_control_toggle').removeClass('hiding_workflow');

			sessionStorage.setItem('workflow_show', true);

		}


		function workflowButtonClicked(event)
		{
			event.preventDefault();
			var workflow_id = $(event.currentTarget).attr('data-id');
			switchToWorkflow(workflow_id);
			var current_section = sessionStorage.getItem('workflow_section_showing');
			showSection(current_section);



		}

		/**
		 * Switches the panel to a given workflow
		 */
		function switchToWorkflow(workflow_id)
		{
			sessionStorage.setItem('workflow_id', workflow_id);

			var $button = $('#workflow_switch_button_' + workflow_id);
			var $data_color = $button.attr('data-color');
			var $data_color_light = $button.attr('data-color-light');

			// Change title color
			$('.workflow_section_box h2 ').css(
				{
					'color':'#'+$data_color,
					'background-color': '#'+$data_color_light,
				});

			$('.TMv_WorkflowControlBar .wf_button').css(
				{
					'color':'#FFF',
					'background-color': '#505050',
				});

			$('.TMv_WorkflowControlBar .wf_button.showing').css(
				{
					'color':'#'+$data_color,
					'background-color': '#'+$data_color_light,
				});

		}

		function showSection(section_name)
		{
			var workflow_id = sessionStorage.getItem('workflow_id');

			$('.workflow_section_box').hide(); // hide all the boxes

			if(section_name == 'settings')
			{
				$('#workflow_' + section_name + '_box').show(); // show the specific workflow related one

			}
			else
			{
				$('#workflow_' + section_name + '_box' + '_' + workflow_id).show(); // show the specific workflow related one

			}

			$('.TMv_WorkflowControlBar .control_button').removeClass('showing');
			$('.TMv_WorkflowControlBar #workflow_control_' + section_name ).addClass('showing');
			sessionStorage.setItem('workflow_section_showing',section_name);

			// Ensure styling updates match
			switchToWorkflow(workflow_id);
		}

		function controlClicked(event)
		{
			event.preventDefault();
			var section_name = $(event.currentTarget).attr('data-section');
			showSection(section_name);
			showPanel();
		}

		function screenScroll()
		{
			// Make sure we're referencing something we aren't moving
			if($('#breadcrumb_submenus').length > 0 && $('#breadcrumb_submenus').offset().top - $(window).scrollTop() < 0)
			{
				$('.TMv_WorkflowControlBar').addClass('pinned');
			}
			else
			{
				$('.TMv_WorkflowControlBar').removeClass('pinned');
			}
			repositionEditor();

		}

		function repositionEditor()
		{


			// position the box fit nicely
			var full_height = $(window).height() - $('.TMv_WorkflowEditor').offset().top + $(window).scrollTop();
			$('.TMv_WorkflowEditor').height(full_height);
		}

		//////////////////////////////////////////////////////
		//
		// HIGHLIGHTING
		//
		//////////////////////////////////////////////////////

		function highlightElement(target_identifier)
		{
			// highlight the new one
			$(target_identifier).addClass('workflow_highlight');


			// remove the class after 2 seconds, animation runs for 2 seconds
			settings.help_timeout = setTimeout(clearHighlights, 4000);
		}

		function clearHighlights()
		{
			$('.workflow_highlight').removeClass('workflow_highlight');
		}


		//////////////////////////////////////////////////////
		//
		// PAGE BUILDER FUNCTIONS
		//
		//////////////////////////////////////////////////////

		function pageBuilderCommentButtonClicked(event)
		{
			event.preventDefault();

			showPanel();
			showSection('comments');

			var content_id = $(event.currentTarget).attr('data-content-id');
			var workflow_id = sessionStorage.getItem('workflow_id');

			$("#comment_link_button_"+workflow_id).attr('data-highlight-target',content_id).slideUp();


			$("#workflow_comment_form_"+workflow_id+" #content_id_"+workflow_id).val(content_id);
			$("#comment_link_button_"+workflow_id).attr('data-highlight-target',content_id).slideDown();

			// Move the cursor
			window.tinyMCE.get('workflow_comment_'+workflow_id).focus();

			highlightElement('#workflow_comment_container');
		}

		function pageBuilderCommentButtonHover(event)
		{
			var content_id = $(event.currentTarget).attr('data-content-id');
			var workflow_id = sessionStorage.getItem('workflow_id');


			// Only show the relevant comments
			$('#related_comments_title_'+workflow_id).show();
			$('.TMv_WorkflowComment').hide();
			$('.TMv_WorkflowComment[data-highlight-target="' + content_id +'"]').show().addClass('show_details');

		}

		function resetCommentList(event)
		{
			var workflow_id = sessionStorage.getItem('workflow_id');
			$('#related_comments_title_'+workflow_id).hide();
			//var content_id = $(event.currentTarget).attr('data-content-id');
			$('.TMv_WorkflowComment').removeClass('show_details').show();


		}

		function contentLinkHover(event)
		{
			var target = $(event.currentTarget).attr('data-highlight-target');
			$('#control_panel_' + target ).addClass('button_hover_workflow_comments button_hover');
		}

		function removeContentLinkHover()
		{
			$('.control_panel').removeClass('button_hover_workflow_comments button_hover');
		}

		function hasCommentLinkClicked(event)
		{
			var workflow_id = sessionStorage.getItem('workflow_id');

			// reset the value, slide up
			$(event.currentTarget).attr('data-highlight-target','').slideUp();

			$("#workflow_comment_form_"+workflow_id+" #content_id_"+workflow_id).val('');
		}



		//////////////////////////////////////////////////////
		//
		// MAIN RETURN
		//
		//////////////////////////////////////////////////////


		return this.each(function()
		{
			// Deal with identifying the first workflow
			$('body').addClass('workflow_init');

			var workflow_id = sessionStorage.getItem('workflow_id');

			// Check for saved workflow that's been disabled
			if(workflow_id)
			{
				if($('#workflow_switch_button_'+workflow_id).length == 0)
				{
					workflow_id = null;
				}
			}

			if(workflow_id == null)
			{
				workflow_id = $($('.workflow_switch_button')[0]).attr('data-id');
			}



			// Ensure color settings are set
			switchToWorkflow(workflow_id);

			// add listeners
			$('#workflow_control_toggle').click(function(event) { toggleClicked(event)});
			$('.TMv_WorkflowControlBar .control_button').click(function(event) { controlClicked(event)});
			$('.workflow_switch_button').click( function(event) { workflowButtonClicked(event); });


			// No section set, default to "status"
			if(settings.start_section == false)
			{
				settings.start_section = 'status';
				var existing_value = sessionStorage.getItem('workflow_section_showing');
				if(existing_value == null)
				{
					settings.start_section = 'status';

				}
				else
				{
					settings.start_section = existing_value;

				}
			}

			showSection(settings.start_section);


			// Deal with showing the workflow at all
			var show_workflow = JSON.parse(sessionStorage.getItem('workflow_show'));
			if(show_workflow == null)
			{
				sessionStorage.setItem('workflow_show', true);
				show_workflow = true;
			}

			if(show_workflow)
			{
				$('body').addClass('showing_workflow');
			}
			else
			{
				$('body').removeClass('showing_workflow');
			}

			$(window).scroll(function()
			{
				screenScroll();
			});

			// Wait a sec for page to load
			setTimeout(screenScroll,1000);


			// Track Page Builder Hover and Click
			var editor = $('.TMv_WorkflowEditor');

			editor.on( "mouseover", ".has_content_link", function(event) { contentLinkHover(event);  });
			editor.on( "mouseout", ".has_content_link", function(event) { removeContentLinkHover(event);  });
			editor.on('click', '.comment_link_button', function(event) { hasCommentLinkClicked(event); });

			var page_builder = $('.TMv_PageBuilder');

			page_builder.on('click', '.content_workflow_comments_button', function(event) { pageBuilderCommentButtonClicked(event); });
			page_builder.on('mouseover', '.content_workflow_comments_button', function(event) { pageBuilderCommentButtonHover(event); });
			page_builder.on('mouseout', '.content_workflow_comments_button', function(event) { resetCommentList(event); });

			// Wait 2 seconds, disable the init, which enables animations
			setTimeout(function(){$('body').removeClass('workflow_init');},1000);

		});


	};


// END THE WRAPPER
})(jQuery);
