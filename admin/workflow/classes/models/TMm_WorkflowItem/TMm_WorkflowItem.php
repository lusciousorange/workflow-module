<?php
class TMm_WorkflowItem extends TCm_Model
{
	protected $title, $workflow_id, $model_name, $model_id, $is_enabled;
	
	protected $stage_models;
	protected $comments;
	protected $users;
	protected $workflow;
	protected $model;
	
	// DATABASE TABLE SETTINGS
	public static $table_id_column = 'item_id'; // [string] = The id column for this class
	public static $table_name = 'workflow_items'; // [string] = The table for this class
	public static $model_title = 'Workflow Item';
	public static $primary_table_sort = 'title ASC';
	
	public function __construct($id, $load_from_table = true)
	{
		parent::__construct($id, $load_from_table);
		
		// Validate that the stage still exists
		if($this->modelName() != '' && $this->modelID() != '' && !$this->model())
		{
			// No stage, this is an orphaned workflow item, just delete it
			$this->delete();
			
			// Ensure the constructor fails
			$this->destroy();
			return false;
		}
	}
	
	public function title()
	{
		if($this->title == '' && $this->model())
		{
			return $this->model()->title();
		}
		return $this->title;
	}
	
	/**
	 * @return bool|TMm_Workflow
	 */
	public function workflow()
	{
		return TMm_Workflow::init($this->workflow_id);
	}
	
	public function modelID()
	{
		return $this->model_id;
	}
	
	/**
	 * Returns the model name for this item
	 * @return string
	 */
	public function modelName()
	{
		return $this->model_name;
	}
	
	/**
	 * @return TCm_Model|bool
	 */
	public function model()
	{
		if(class_exists($this->model_name))
		{
			return ($this->model_name)::init($this->model_id);
		}
		return false;
		
	}
	
	/**
	 * Returns if the item is enabled
	 * @return bool
	 */
	public function isEnabled()
	{
		return $this->is_enabled;
	}
	
	/**
	 * Enables this workflow item
	 */
	public function enable()
	{
		$this->updateDatabaseValue('is_enabled', 1);
	}
	
	/**
	 * Disables this workflow item
	 */
	public function disable()
	{
		$this->updateDatabaseValue('is_enabled', 0);
	}
	

	/**
	 * @return TMm_WorkflowModelStage[]
	 */
	public function stageModelHistory()
	{
		if($this->stage_models == null)
		{
			$this->stage_models = array();
			$module = TMm_WorkflowModule::init();
			
			// Get all the stages that this model is associated with
			$stages = $module->stagesForModel($this->model());
			// filter out the ones that are associated with this workflow
			foreach($stages as $potential_stage)
			{
				if($potential_stage->stage()->workflow()->id() == $this->workflow()->id() )
				{
					$this->stage_models[] = $potential_stage;
				}
				
				
			}
			
			// Generate the first one if there aren't any
			if(count($this->stage_models) == 0)
			{
				$model_stage = $module->createFirstModelStageForWorkflowItem($this);
				$this->stage_models[] = $model_stage;
				
			}
			
		}
		
		return $this->stage_models;
	}
	
	/**
	 * @return bool|TMm_WorkflowModelStage
	 */
	public function currentStageModel()
	{
		$stages = array_values($this->stageModelHistory());
		if(count($stages) > 0)
		{
			return $stages[0];
		}
		
		return false;
	}
	
	/**
	 * @return TMm_WorkflowComment[]
	 */
	public function comments()
	{
		if($this->comments == null)
		{
			$this->comments = array();
			$module = TMm_WorkflowModule::init();
			$comments = $module->commentsForModel($this->model());
			
			// filter out the ones that are associated with this workflow
			foreach($comments as $comment)
			{
				if($comment->workflow()->id() == $this->workflow()->id() )
				{
					$this->comments[] = $comment;
				}
			}
			
		}
		
		return $this->comments;
	}

	/**
	 * @return TMm_WorkflowUser[]
	 */
	public function workflowUsers()
	{
		if($this->users == null)
		{
			$this->users = array();
			$module = TMm_WorkflowModule::init();


			// filter out the ones that are associated with this workflow
			foreach($module->usersForModel($this->model()) as $user)
			{
				if($user->workflow()->id() == $this->workflow()->id() )
				{
					$this->users[] = $user;
				}
			}

		}

		return $this->users;
	}


}