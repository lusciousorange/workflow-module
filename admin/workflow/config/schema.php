<?php

	// ----- WORKFLOW -----
	$tables['workflows']['primary_key'] = 'workflow_id';
	$tables['workflows']['columns'] = array(
		"title text NOT NULL",
		"code varchar(3) NOT NULL",
		"color varchar(6) NOT NULL",
		"model_class_names text NOT NULL",
		"description text NOT NULL",
		"is_user_restricted tinyint(1) unsigned NOT NULL",
		"date_added datetime NOT NULL",
		"user_id int(10) unsigned NOT NULL",
	);

	// ----- STAGES -----
	$tables['workflow_stages']['primary_key'] = 'stage_id';
	$tables['workflow_stages']['columns'] = array(
		"title text NOT NULL",
		"workflow_id int(10) unsigned NOT NULL DEFAULT 1",
		"description text NOT NULL",
		"date_added datetime NOT NULL",
		"color char(6) NOT NULL ",
		"display_order int(10) unsigned NOT NULL",
	);
	$tables['workflow_stages']['foreign_key']['workflow_id'] = 'workflows';

	// ----- ITEMS -----
	$tables['workflow_items']['primary_key'] = 'item_id';
	$tables['workflow_items']['columns'] = array(
		"title text NOT NULL",
		"workflow_id int(10) unsigned NOT NULL DEFAULT 1",
		"date_added datetime NOT NULL",
		"user_id int(10) unsigned NOT NULL",
		"model_name text NOT NULL",
		"model_id int(10) NOT NULL",
		"display_order int(10) unsigned NOT NULL",
		"is_enabled tinyint(1) unsigned NOT NULL DEFAULT 1",
	
	);
	$tables['workflow_items']['foreign_key']['workflow_id'] = 'workflows';



// ----- COMMENTS -----
	$tables['workflow_model_comments']['primary_key'] = 'comment_id';
	$tables['workflow_model_comments']['columns'] = array(
		"comment longtext NOT NULL",
		"date_added datetime NOT NULL",
		"user_id int(10) unsigned NOT NULL",
		"is_resolved int(1) unsigned NOT NULL",
		"model_name text NOT NULL",
		"model_id int(10) NOT NULL",
		"workflow_id int(10) unsigned NOT NULL DEFAULT 1",
		"content_id int(10) unsigned"
	);

	$tables['workflow_model_comments']['foreign_key']['user_id'] = 'users';
	$tables['workflow_model_comments']['foreign_key']['content_id'] = 'pages_content';
	$tables['workflow_model_comments']['foreign_key']['workflow_id'] = 'workflows';

// ----- COMMENTS -----
	$tables['workflow_model_users']['primary_key'] = 'match_id';
	$tables['workflow_model_users']['columns'] = array(
		"date_added datetime NOT NULL",
		"user_id int(10) unsigned NOT NULL",
		"model_name text NOT NULL",
		"model_id int(10) NOT NULL",
		"workflow_id int(10) unsigned NOT NULL DEFAULT 1",
	);

	$tables['workflow_model_users']['foreign_key']['user_id'] = 'users';
	$tables['workflow_model_users']['foreign_key']['workflow_id'] = 'workflows';


	// ----- COMMENTS -----
	$tables['workflow_model_stages']['primary_key'] = 'match_id';
	$tables['workflow_model_stages']['columns'] = array(
		"stage_id int(10) unsigned NOT NULL",
		"date_added datetime NOT NULL",
		"user_id int(10) unsigned NOT NULL",
		"model_name text NOT NULL",
		"model_id int(10) NOT NULL",
	);

	$tables['workflow_model_stages']['foreign_key']['user_id'] = 'users';


	

	$tables['workflow_stage_user_group_matches']['primary_key'] = 'match_id';
	$tables['workflow_stage_user_group_matches']['columns'] = array(
		"stage_id int(10) unsigned NOT NULL",
		"group_id int(10) unsigned NOT NULL",
		"date_added datetime NOT NULL");
	$tables['workflow_stage_user_group_matches']['foreign_key']['stage_id'] = 'workflow_stages';
	$tables['workflow_stage_user_group_matches']['foreign_key']['group_id'] = 'user_groups';


	$tables['workflow_stage_next_stage_matches']['primary_key'] = 'match_id';
	$tables['workflow_stage_next_stage_matches']['columns'] = array(
		"stage_id int(10) unsigned NOT NULL",
		"next_stage_id int(10) unsigned NOT NULL",
		"date_added datetime NOT NULL");
	$tables['workflow_stage_user_group_matches']['foreign_key']['stage_id'] = 'workflow_stages';
	//$tables['workflow_stage_user_group_matches']['foreign_key']['next_stage_id'] = 'workflow_stages';

	// ALTER USERS TABLE FOR SETTINGS
	$tables['users']['columns'] = array(
		"workflow_email_comments int(1) unsigned NOT NULL DEFAULT '1' ",
		"workflow_email_stages int(1) unsigned NOT NULL DEFAULT '1' ",
		"workflow_email_assigned int(1) unsigned NOT NULL DEFAULT '1' ",



	);
?>