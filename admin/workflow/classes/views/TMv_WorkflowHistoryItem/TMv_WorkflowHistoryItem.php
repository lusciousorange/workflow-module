<?php

/**
 * Class TMv_WorkflowHistoryItem
 */
class TMv_WorkflowHistoryItem extends TCv_View
{
	protected $history_item = false;
	protected $show_item_title = false;

	/**
	 * TMv_WorkflowHistoryItem constructor.
	 * @param TMm_WorkflowHistoryItem $history_item
	 */
	public function __construct ($history_item)
	{
		$this->history_item = $history_item;
		parent::__construct($history_item->contentCode());

			$this->addClassCSSFile('TMv_WorkflowHistoryItem?v1');



	}

	public function setShowItemTitle()
	{
		$this->show_item_title = true;
	}

	public function showItemTitle()
	{
		return $this->show_item_title ;
	}

	public function item()
	{
		return $this->history_item;
	}

	public function modelLink()
	{
		$model = $this->item()->model();
		$link = new TCv_Link();
		$link->addClass('workflow_model_link');
		$link->addText($model->title());
		$link->setIconClassName($model->moduleForThisClass()->iconCode());
		$link->setURL($model->adminEditURL());
		return $link;
	}

	public function html ()
	{

		return parent::html();
	}

}